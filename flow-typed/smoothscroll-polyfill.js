declare module 'smoothscroll-polyfill' {
    declare function smoothscroll (): void;

    declare export default typeof smoothscroll;
};
