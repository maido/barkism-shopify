declare module 'flickity' {
    declare function flickity (element: HTMLElement, config: Object): Function;

    declare export default typeof flickity;
};
