declare module 'sticky-js' {
    declare function Sticky (selector: string): void;

    declare export default typeof Sticky;
};
