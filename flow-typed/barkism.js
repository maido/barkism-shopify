type DogInformation = {
    name: string,
    breed: string,
    birthday: string,
    instagram: string,
    fussiness: string,
    toy_destroy: string
};

type Feedback = {
    label?: string,
    text: string,
    theme?: string
};

type Notification = {
    key: string,
    label: string,
    text: string,
};
