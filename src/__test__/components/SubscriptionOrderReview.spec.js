/**
 * @prettier
 */
import React from 'react';
import {shallow} from 'enzyme';
import renderer from 'react-test-renderer';
import data from '../data/products';
import SubscriptionOrderReview from '../../js/components/react/components/SubscriptionOrderReview';

describe('SubscriptionOrderReview', () => {
    test('it renders correctly for subscription products', () => {
        const tree = renderer
            .create(
                <SubscriptionOrderReview
                    type="subscription"
                    defaultPrice={2500}
                    formSubmitted={false}
                    formDisabled={true}
                    product={data.subscription}
                    copy={{}}
                    images={{cards: ''}}
                />
            )
            .toJSON();

        expect(tree).toMatchSnapshot();
    });

    test('it renders correctly for gift products', () => {
        const tree = renderer
            .create(
                <SubscriptionOrderReview
                    type="gift"
                    defaultPrice={2500}
                    formSubmitted={false}
                    formDisabled={true}
                    product={data.gift1}
                    copy={{}}
                    images={{cards: ''}}
                />
            )
            .toJSON();

        expect(tree).toMatchSnapshot();
    });

    test('it renders the product id in a hidden field', () => {
        const $wrapper = shallow(
            <SubscriptionOrderReview
                type="gift"
                defaultPrice={2500}
                formSubmitted={false}
                formDisabled={true}
                product={data.gift1}
                copy={{}}
                images={{cards: ''}}
            />
        );

        expect($wrapper.find('input[name="id"]').props().value).toEqual(data.gift1.id);
    });

    test('it renders clear information for subscription renewals and cancellation', () => {
        const $wrapper = shallow(
            <SubscriptionOrderReview
                type="subscription"
                defaultPrice={2500}
                formSubmitted={false}
                formDisabled={true}
                product={data.subscription}
                copy={{}}
                images={{cards: ''}}
            />
        );

        $wrapper.find('.js-info-toggle').simulate('click');

        expect($wrapper.find('.js-subscription-renew').exists()).toEqual(true);

        const $wrapper2 = shallow(
            <SubscriptionOrderReview
                type="gift"
                defaultPrice={2500}
                formSubmitted={false}
                formDisabled={true}
                product={data.gift1}
                copy={{}}
                images={{cards: ''}}
            />
        );

        expect($wrapper2.find('.js-subscription-renew').exists()).toEqual(false);
    });

    test('it renders a disabled submit button if the form is not complete', () => {
        const $wrapper = shallow(
            <SubscriptionOrderReview
                type="subscription"
                defaultPrice={2500}
                formSubmitted={false}
                formDisabled={true}
                product={data.subscription}
                copy={{}}
                images={{cards: ''}}
            />
        );

        expect($wrapper.find('button[type="submit"]').props().disabled).toEqual(true);

        const $wrapper2 = shallow(
            <SubscriptionOrderReview
                type="subscription"
                defaultPrice={2500}
                formSubmitted={false}
                formDisabled={false}
                product={data.subscription}
                copy={{}}
                images={{cards: ''}}
            />
        );

        expect($wrapper2.find('button[type="submit"]').props().disabled).toEqual(false);
    });

    test('it renders a disabled submit button if the form has been completed and then submitted', () => {
        const $wrapper = shallow(
            <SubscriptionOrderReview
                type="subscription"
                defaultPrice={2500}
                formSubmitted={false}
                formDisabled={false}
                product={data.subscription}
                copy={{}}
                images={{cards: ''}}
            />
        );

        expect($wrapper.find('button[type="submit"]').props().disabled).toEqual(false);

        const $wrapper2 = shallow(
            <SubscriptionOrderReview
                type="subscription"
                defaultPrice={2500}
                formSubmitted={true}
                formDisabled={false}
                product={data.subscription}
                copy={{}}
                images={{cards: ''}}
            />
        );

        expect($wrapper2.find('button[type="submit"]').props().disabled).toEqual(true);
    });

    test('it renders an opaque submit button if the form has not been completed', () => {
        const $wrapper = shallow(
            <SubscriptionOrderReview
                type="subscription"
                defaultPrice={2500}
                formSubmitted={false}
                formDisabled={false}
                product={data.subscription}
                copy={{}}
                images={{cards: ''}}
            />
        );

        expect($wrapper.find('button[type="submit"]').props().style.opacity).toEqual(1);

        const $wrapper2 = shallow(
            <SubscriptionOrderReview
                type="subscription"
                defaultPrice={2500}
                formSubmitted={true}
                formDisabled={false}
                product={data.subscription}
                copy={{}}
                images={{cards: ''}}
            />
        );

        expect($wrapper2.find('button[type="submit"]').props().style.opacity).toEqual(0.5);
    });

    test('it renders a default price if no product is available', () => {
        const $wrapper = shallow(
            <SubscriptionOrderReview
                type="subscription"
                defaultPrice={5000}
                formSubmitted={false}
                formDisabled={false}
                product={null}
                copy={{}}
                images={{cards: ''}}
            />
        );

        expect($wrapper.find('.js-price').text()).toEqual('£50/month');
    });

    test('it renders a formatted price for subscriptions', () => {
        const $wrapper = shallow(
            <SubscriptionOrderReview
                type="subscription"
                defaultPrice={2500}
                formSubmitted={false}
                formDisabled={false}
                product={data.subscription}
                copy={{}}
                images={{cards: ''}}
            />
        );

        expect($wrapper.find('.js-price').text()).toEqual('£25/month');
    });

    test('it renders a formatted price for gifts', () => {
        const $wrapper = shallow(
            <SubscriptionOrderReview
                type="gift"
                defaultPrice={2500}
                formSubmitted={false}
                formDisabled={false}
                product={data.gift1}
                copy={{}}
                images={{cards: ''}}
            />
        );

        expect($wrapper.find('.js-price').text()).toEqual('£25');
    });
});
