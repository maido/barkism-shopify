/**
 * @prettier
 */
import 'isomorphic-fetch';
import React from 'react';
import {mount} from 'enzyme';
import {spy} from 'sinon';
import fetchMock from 'fetch-mock';
import renderer from 'react-test-renderer';
import data, {subscriptionProducts} from '../data/products';
import * as modal from '../../js/components/modal';
import Subscription from '../../js/components/react/Subscription';

describe('Subscription', () => {
    let clearMock;

    const rechargeElementIds = [
        'rc_subscription_id',
        'rc_shipping_interval_unit_type',
        'rc_shipping_interval_frequency',
        'rc_duplicate_selector'
    ];

    beforeEach(() => {
        window.ReCharge = {
            products: [
                {
                    id: 1422124580982,
                    subscription_id: 128336,
                    shop_currency: 'GBP',
                    currency_prefix: '£',
                    currency_suffix: '',
                    money_format: '£{{amount}}',
                    subscription_only: true,
                    select_subscription_first: false,
                    shipping_interval_unit_type: 'Months',
                    shipping_interval_frequency: [1],
                    discount_percentage: 0,
                    variant_to_duplicate: {'13150869717110': '18283051155574'},
                    variant_to_price: {'13150869717110': '100'},
                    duplicate_to_price: {'18283051155574': '100'},
                    options: {
                        active: true,
                        ready: true,
                        debug: false,
                        price_limiter: 3,
                        dom_step_limiter: 3,
                        delay_listener: 1,
                        select_subscription_first: false,
                        money_format: '£{{amount}}',
                        currency_prefix: '£',
                        currency_suffix: '',
                        shop_currency: 'GBP',
                        update_pricing: true,
                        disable_ajax: false,
                        currency_conversion: true,
                        disable_duplicates: false,
                        id: 1422124580982,
                        subscription_id: 128336,
                        subscription_only: true,
                        discount_percentage: 0,
                        variant_to_duplicate: {'13150869717110': '18283051155574'},
                        variant_to_price: {'13150869717110': '100'},
                        duplicate_to_price: {'18283051155574': '100'},
                        form_selector: null,
                        price_selector: null,
                        options_selector: null,
                        shipping_interval_unit_type: 'Months',
                        shipping_interval_frequency: [1],
                        purchaseType: 'autodeliver'
                    },
                    status: 'complete',
                    forms: [{'0': {}, '1': {}, '2': {'0': {}}, '3': {}, '4': {}, '5': {}}],
                    elements: [
                        {
                            rcContainer: {},
                            purchaseTypes: {'0': {}},
                            radioOnetime: null,
                            radioAutodeliver: {},
                            subscriptionId: {},
                            subscriptionIntervalType: {},
                            shippingIntervalFrequency: {},
                            intervalOptions: {},
                            productVariantSelect: {},
                            duplicateVariantSelect: {'0': {}},
                            onetimePrice: null,
                            autodeliverPrice: null,
                            form: {'0': {}, '1': {}, '2': {'0': {}}, '3': {}, '4': {}, '5': {}},
                            activePurchaseType: {},
                            activeProductSelect: {'0': {}},
                            productPrice: [],
                            variantInputs: [{}, {}, {}]
                        }
                    ]
                }
            ]
        };

        const elementIds = [
            'rc_subscription_id',
            'rc_shipping_interval_unit_type',
            'rc_shipping_interval_frequency',
            'rc_duplicate_selector'
        ];

        rechargeElementIds.map(id => {
            const $el = document.createElement('input');

            $el.setAttribute('id', id);
            document.body.appendChild($el);
        });

        clearMock = fetchMock.mock(`${window.globalData.rootURL}/cart/clear.js`, {
            status: 200,
            body: JSON.stringify({})
        });
    });

    afterEach(() => {
        fetchMock.reset();
        fetchMock.restore();

        delete window.ReCharge;
    });

    test('it renders correctly for subscription products', () => {
        const tree = renderer
            .create(
                <Subscription
                    userStatus="none"
                    product={data.subscription}
                    allProducts={data}
                    subscriptionProducts={subscriptionProducts}
                />
            )
            .toJSON();

        expect(tree).toMatchSnapshot();
    });

    test('it renders correctly for gift products', () => {
        const tree = renderer
            .create(
                <Subscription
                    userStatus="none"
                    product={data.gift1}
                    allProducts={data}
                    subscriptionProducts={subscriptionProducts}
                />
            )
            .toJSON();

        expect(tree).toMatchSnapshot();
    });

    test('it clears any existing items in the cart on load', () => {
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.subscription}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        expect(clearMock.lastCall()[0]).toEqual(`${window.globalData.rootURL}/cart/clear.js`);
    });

    test('it renders all questions required for subscriptions', () => {
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.subscription}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper
            .find('SubscriptionFormStep')
            .at(0)
            .find('button')
            .at(0)
            .simulate('click');

        expect($wrapper.find('.js-step[data-active=true]').length).toEqual(3);
    });

    test('it renders all questions required for gifts', () => {
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper
            .find('SubscriptionFormStep')
            .at(0)
            .find('button')
            .at(1)
            .simulate('click');

        expect($wrapper.find('.js-step[data-active=true]').length).toEqual(5);
    });

    test('it shows a `manage subscription` message if the user has an existing subscription', () => {
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        expect($wrapper.find('SubscriptionReviewBanner').length).toEqual(0);

        const $wrapper2 = mount(
            <Subscription
                userStatus="active"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        expect($wrapper2.find('SubscriptionReviewBanner').length).toEqual(1);
    });

    test('it disables the full form if the user is shown the `manage subscription` message', () => {
        const $wrapper = mount(
            <Subscription
                userStatus="active"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        expect($wrapper.state().steps).toEqual({
            completed: 0,
            current: 0,
            previous: 0
        });
    });

    test('it allows the user to continue with creating a new subscription after reading the review', () => {
        const $wrapper = mount(
            <Subscription
                userStatus="active"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper
            .find('SubscriptionReviewBanner')
            .find('.js-continue')
            .simulate('click');

        expect($wrapper.state().steps).toEqual({
            completed: 1,
            current: 2,
            previous: 1
        });
    });

    test('it hides the subscription management banner after continuing with subscription', () => {
        const $wrapper = mount(
            <Subscription
                userStatus="active"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper
            .find('SubscriptionReviewBanner')
            .find('.js-continue')
            .simulate('click');

        expect($wrapper.find('SubscriptionReviewBanner').length).toEqual(0);
    });

    test('it stores the product type when it is selected', () => {
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper
            .find('SubscriptionFormStep')
            .at(0)
            .find('button')
            .at(0)
            .simulate('click');

        expect($wrapper.state().type).toEqual('subscription');

        $wrapper
            .find('SubscriptionFormStep')
            .at(0)
            .find('button')
            .at(1)
            .simulate('click');

        expect($wrapper.state().type).toEqual('gift');
    });

    test("it stores the dog's size when it is selected", () => {
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper
            .find('SubscriptionFormStep')
            .at(1)
            .find('button')
            .at(1)
            .simulate('click');

        expect($wrapper.state().dogSize).toEqual('Small');
    });

    test('it renders a modal for `size guide`, explaining supported sizes', () => {
        const openModalSpy = spy(modal, 'openModal');
        const setActiveModalSpy = spy(modal, 'setActiveModal');
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper
            .find('SubscriptionFormStep')
            .at(1)
            .find('button')
            .at(0)
            .simulate('click');

        expect(openModalSpy.called).toEqual(true);
        expect(openModalSpy.calledWith(true)).toEqual(true);
        expect(setActiveModalSpy.called).toEqual(true);
        expect(setActiveModalSpy.calledWith('dog-sizes-guide')).toEqual(true);

        openModalSpy.restore();
        setActiveModalSpy.restore();
    });

    test('it renders a modal for `big` dogs, explaining that they are not supported yet', () => {
        const openModalSpy = spy(modal, 'openModal');
        const setActiveModalSpy = spy(modal, 'setActiveModal');
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper
            .find('SubscriptionFormStep')
            .at(1)
            .find('button')
            .at(2)
            .simulate('click');

        expect(openModalSpy.called).toEqual(true);
        expect(openModalSpy.calledWith(true)).toEqual(true);
        expect(setActiveModalSpy.called).toEqual(true);
        expect(setActiveModalSpy.calledWith('box-sizes-fallback')).toEqual(true);

        openModalSpy.restore();
        setActiveModalSpy.restore();
    });

    test("it stores the dog's name when it is added", () => {
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );
        const $input = $wrapper
            .find('SubscriptionFormStep')
            .at(2)
            .find('input[type="text"]')
            .at(0);

        $input.instance().value = 'Chip';
        $input.simulate('change');

        expect($wrapper.state().dogName).toEqual('Chip');
    });

    test("it stores the dog's breed when it is added", () => {
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );
        const $input = $wrapper
            .find('SubscriptionFormStep')
            .at(2)
            .find('input[type="text"]')
            .at(1);

        $input.instance().value = 'Daxi';
        $input.simulate('change');

        expect($wrapper.state().dogBreed).toEqual('Daxi');
    });

    test('it stores the gift duration when it is selected', () => {
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper.setState({steps: {completed: 4, current: 5}});
        $wrapper
            .find('SubscriptionFormStep')
            .at(3)
            .find('button')
            .at(1)
            .simulate('click');

        expect($wrapper.state().duration).toEqual(2);

        $wrapper
            .find('SubscriptionFormStep')
            .at(3)
            .find('button')
            .at(0)
            .simulate('click');

        expect($wrapper.state().duration).toEqual(1);

        $wrapper
            .find('SubscriptionFormStep')
            .at(3)
            .find('button')
            .at(2)
            .simulate('click');

        expect($wrapper.state().duration).toEqual(4);
    });

    test('it stores the gift message when it is added', () => {
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper.setState({type: 'gift', steps: {completed: 4, current: 5}});

        const $input = $wrapper
            .find('SubscriptionFormStep')
            .at(4)
            .find('textarea');

        $input.instance().value = 'Enjoy!';
        $input.simulate('change');

        expect($wrapper.state().message).toEqual('Enjoy!');
    });

    test('it disables the form until subscription data is complete', () => {
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        expect($wrapper.state().formComplete).toEqual(false);

        $wrapper
            .find('SubscriptionFormStep')
            .at(1)
            .find('button')
            .at(0)
            .simulate('click');

        expect($wrapper.state().formComplete).toEqual(false);

        const $nameInput = $wrapper
            .find('SubscriptionFormStep')
            .at(2)
            .find('input[type="text"]')
            .at(0);

        $nameInput.instance().value = 'Chip';
        $nameInput.simulate('change');

        expect($wrapper.state().formComplete).toEqual(false);

        const $breedInput = $wrapper
            .find('SubscriptionFormStep')
            .at(2)
            .find('input[type="text"]')
            .at(1);

        $breedInput.instance().value = 'Daxi';
        $breedInput.simulate('change');

        expect($wrapper.state().formComplete).toEqual(true);
    });

    test('it disables the form until gift data is complete', () => {
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        expect($wrapper.state().formComplete).toEqual(false);

        $wrapper
            .find('SubscriptionFormStep')
            .at(0)
            .find('button')
            .at(1)
            .simulate('click');

        expect($wrapper.state().formComplete).toEqual(false);

        $wrapper
            .find('SubscriptionFormStep')
            .at(1)
            .find('button')
            .at(0)
            .simulate('click');

        expect($wrapper.state().formComplete).toEqual(false);

        const $nameInput = $wrapper
            .find('SubscriptionFormStep')
            .at(2)
            .find('input[type="text"]')
            .at(0);

        $nameInput.instance().value = 'Chip';
        $nameInput.simulate('change');

        expect($wrapper.state().formComplete).toEqual(false);

        const $breedInput = $wrapper
            .find('SubscriptionFormStep')
            .at(2)
            .find('input[type="text"]')
            .at(1);

        $breedInput.instance().value = 'Daxi';
        $breedInput.simulate('change');

        expect($wrapper.state().formComplete).toEqual(false);

        $wrapper
            .find('SubscriptionFormStep')
            .at(3)
            .find('button')
            .at(1);

        expect($wrapper.state().formComplete).toEqual(false);

        const $messageInput = $wrapper
            .find('SubscriptionFormStep')
            .at(4)
            .find('textarea');

        $messageInput.instance().value = 'Enjoy!';
        $messageInput.simulate('change');

        expect($wrapper.state().formComplete).toEqual(true);
    });

    test("it updates the recharge subscription information to match the users' selection", done => {
        const addMock = fetchMock.mock(`${window.globalData.rootURL}/cart/add.js`, {
            status: 200,
            body: JSON.stringify({variant_id: data.gift2.variants[0].id})
        });
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.subscription}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper.setState({
            type: 'gift',
            duration: 2,
            dogSize: 'small',
            dogName: 'Chip',
            dogBreed: 'Daxi',
            formComplete: true,
            hasReviewedGiftShipping: true,
            productVariant: data.gift2.variants[0],
            selectedProductId: data.gift2.id
        });

        $wrapper.find('form').simulate('submit');

        setTimeout(() => {
            expect(document.querySelector('#rc_subscription_id').value).toEqual(
                subscriptionProducts[data.gift2.id].rc_subscription_id
            );
            expect(document.querySelector('#rc_shipping_interval_unit_type').value).toEqual(
                subscriptionProducts[data.gift2.id].rc_shipping_interval_unit_type
            );
            expect(document.querySelector('#rc_shipping_interval_frequency').value).toEqual(
                subscriptionProducts[data.gift2.id].rc_shipping_interval_frequency
            );
            expect(document.querySelector('#rc_duplicate_selector').value).toEqual(
                subscriptionProducts[data.gift2.id].rc_duplicate_selector
            );

            done();
        });
    });

    test('it submits the subscription form data when it has been completed', () => {
        const addMock = fetchMock.mock(`${window.globalData.rootURL}/cart/add.js`, {
            status: 200,
            body: JSON.stringify({variant_id: data.subscription.variants[0].id})
        });
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.subscription}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper.setState({
            type: 'subscription',
            dogSize: 'small',
            dogName: 'Chip',
            dogBreed: 'Daxi',
            formComplete: true,
            productVariant: data.subscription.variants[0]
        });
        $wrapper.find('form').simulate('submit');

        expect(addMock.lastCall()[0]).toEqual(`${window.globalData.rootURL}/cart/add.js`);

        const request = JSON.parse(addMock.lastCall()[1].body);

        expect(request.id).toEqual($wrapper.state().productVariant.id);
        expect(request.quantity).toEqual(1);
        expect(request.properties.dog_name).toEqual($wrapper.state().dogName);
        expect(request.properties.dog_breed).toEqual($wrapper.state().dogBreed);
        expect(request.properties.shipping_interval_frequency).toEqual(1);
        expect(request.properties.shipping_interval_unit_type).toEqual('Months');
        expect(typeof request.properties.subscription_id).toEqual('number');
    });

    test('it renders a modal to review a gift order before submitting', () => {
        const openModalSpy = spy(modal, 'openModal');
        const setActiveModalSpy = spy(modal, 'setActiveModal');
        const addMock = fetchMock.mock(`${window.globalData.rootURL}/cart/add.js`, {
            status: 200,
            body: JSON.stringify({variant_id: data.gift1.variants[0].id})
        });
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift1}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper.setState({
            type: 'gift',
            duration: 2,
            message: 'Enjoy!',
            dogSize: 'small',
            dogName: 'Chip',
            dogBreed: 'Daxi',
            formComplete: true,
            productVariant: data.gift2.variants[0]
        });
        $wrapper.find('form').simulate('submit');

        expect(openModalSpy.called).toEqual(true);
        expect(openModalSpy.calledWith(true)).toEqual(true);
        expect(setActiveModalSpy.called).toEqual(true);
        expect(setActiveModalSpy.calledWith('gift-subscription-review')).toEqual(true);

        openModalSpy.restore();
        setActiveModalSpy.restore();
    });

    test('it submits the gift form data when it has been completed and reviewed', () => {
        const addMock = fetchMock.mock(`${window.globalData.rootURL}/cart/add.js`, {
            status: 200,
            body: JSON.stringify({variant_id: data.gift2.variants[0].id})
        });
        const $wrapper = mount(
            <Subscription
                userStatus="none"
                product={data.gift2}
                allProducts={data}
                subscriptionProducts={subscriptionProducts}
            />
        );

        $wrapper.setState({
            type: 'gift',
            duration: 2,
            message: 'Enjoy!',
            dogSize: 'small',
            dogName: 'Chip',
            dogBreed: 'Daxi',
            formComplete: true,
            productVariant: data.gift2.variants[0]
        });
        $wrapper.find('form').simulate('submit');

        expect($wrapper.state().hasReviewedGiftShipping).toEqual(true);

        // Submit a second time, after the modal has been shown
        $wrapper.find('form').simulate('submit');

        expect(addMock.lastCall()[0]).toEqual(`${window.globalData.rootURL}/cart/add.js`);

        const request = JSON.parse(addMock.lastCall()[1].body);

        expect(request.id).toEqual($wrapper.state().productVariant.id);
        expect(request.quantity).toEqual(1);
        expect(request.properties.dog_name).toEqual($wrapper.state().dogName);
        expect(request.properties.dog_breed).toEqual($wrapper.state().dogBreed);
        expect(request.properties.message).toEqual($wrapper.state().message);
    });
});
