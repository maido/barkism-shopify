/**
 * @prettier
 */
import React from 'react';
import {shallow} from 'enzyme';
import renderer from 'react-test-renderer';
import {advanceTo, clear} from 'jest-date-mock';
import {
    SUBSCRIPTION_SHIPPING_DAY,
    SUBSCRIPTION_CUTOFF_DAY,
    SUBSCRIPTION_CHARGE_DAY
} from '../../js/config';
import SubscriptionNextShippingDate from '../../js/components/react/components/SubscriptionNextShippingDate';

describe('SubscriptionNextShippingDate', () => {
    afterEach(() => clear());

    test('it renders the default shipping date of the current month, if the current day is before the default shipping day', () => {
        advanceTo(new Date(2018, 7, 1, 0, 0, 0));

        const $el = shallow(<SubscriptionNextShippingDate />);
        expect($el.text()).toEqual('Your Barkism box will ship on August 7th.');
    });

    test('it renders the default `x working days` notice, if the current day is equal to the default shipping day of the current month, but before the cut off day', () => {
        advanceTo(new Date(2018, 7, SUBSCRIPTION_SHIPPING_DAY, 0, 0, 0));

        const $el = shallow(<SubscriptionNextShippingDate />);
        expect($el.text()).toEqual('Your Barkism box will ship within 2 working days.');
    });

    test('it renders the default `x working days` notice, if the current day is after the default shipping day of the current month, but before the cut off day', () => {
        advanceTo(new Date(2018, 7, SUBSCRIPTION_SHIPPING_DAY + 1, 0, 0, 0));

        const $el = shallow(<SubscriptionNextShippingDate />);
        expect($el.text()).toEqual('Your Barkism box will ship within 2 working days.');
    });

    test('it renders the next month`s default shipping date, if the current day is equal to the cutoff day of the current month', () => {
        advanceTo(new Date(2018, 7, SUBSCRIPTION_CUTOFF_DAY, 0, 0, 0));

        const $el = shallow(<SubscriptionNextShippingDate />);
        expect($el.text()).toEqual('Your first Barkism box will ship on September 7th.');
    });

    test('it renders the next month`s default shipping date, if the current day is after the cutoff day of the current month', () => {
        advanceTo(new Date(2018, 7, SUBSCRIPTION_CUTOFF_DAY + 1, 0, 0, 0));

        const $el = shallow(<SubscriptionNextShippingDate />);
        expect($el.text()).toEqual('Your first Barkism box will ship on September 7th.');
    });
});
