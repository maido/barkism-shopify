/**
 * @prettier
 */
import React from 'react';
import {shallow} from 'enzyme';
import renderer from 'react-test-renderer';
import SubscriptionFormStep from '../../js/components/react/components/SubscriptionFormStep';

describe('SubscriptionFormStep', () => {
    test('it renders correctly', () => {
        const tree = renderer.create(<SubscriptionFormStep />).toJSON();

        expect(tree).toMatchSnapshot();
    });

    test('it renders child component as a function', () => {
        const $el = shallow(
            <SubscriptionFormStep isActive={false}>
                {isActive => <div className="js-test">Foo</div>}
            </SubscriptionFormStep>
        );

        expect($el.find('.js-test').text()).toEqual('Foo');
    });

    test('it renders child component as a function, with an active boolean', () => {
        const $el = shallow(
            <SubscriptionFormStep isActive={true}>
                {isActive => <div className="js-test">{isActive ? 'foo' : null}</div>}
            </SubscriptionFormStep>
        );

        expect($el.find('.js-test').text()).toEqual('foo');

        const $el2 = shallow(
            <SubscriptionFormStep isActive={false}>
                {isActive => <div className="js-test">{isActive ? 'foo' : null}</div>}
            </SubscriptionFormStep>
        );

        expect($el2.find('.js-test').text()).toEqual('');
    });

    test('it renders a hidden container if the step is not visible', () => {
        const $el = shallow(
            <SubscriptionFormStep isActive={true}>
                {isActive => <div className="js-test">{isActive ? 'foo' : null}</div>}
            </SubscriptionFormStep>
        );

        expect($el.find('.js-test').text()).toEqual('foo');

        const $el2 = shallow(
            <SubscriptionFormStep isActive={false}>
                {isActive => <div className="js-test">{isActive ? 'foo' : null}</div>}
            </SubscriptionFormStep>
        );

        expect($el2.find('.js-test').text()).toEqual('');
    });

    test('it renders an `event blocking` container if the step is not active, to prevent any interaction with it', () => {
        const $el = shallow(
            <SubscriptionFormStep isActive={false}>{isActive => <div />}</SubscriptionFormStep>
        );

        expect($el.find('.js-step-blocker').prop('style').visibility).toEqual('visible');
    });

    test('it renders an opaque container if the step is not active', () => {
        const $el = shallow(
            <SubscriptionFormStep isActive={false}>{isActive => <div />}</SubscriptionFormStep>
        );

        expect($el.find('.js-step').prop('style').opacity).toEqual(0.4);
    });
});
