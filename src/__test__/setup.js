import Enzyme from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

Enzyme.configure({adapter: new EnzymeAdapter()});

// global.fetch = require('jest-fetch-mock');

window.scrollBy = () => {};

window.globalData = {rootURL:'https://localhost:8000'}
window.localStorage = {};
window.scroll = () => {};

window.alert = message => console.warn(`ALERT: ${message}`);
window.reChargeProcessCart = () => {};
