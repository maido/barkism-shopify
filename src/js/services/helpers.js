/**
 * @prettier
 * @/flow
 */
import round from 'lodash/round';

/**
 * Still figuring out Flow types - will come back to this!
 * - For recursive functions, how do we define return types for a specific value or the function?
 * - How can HTMLElement types reference their 'possibly null' parentNode values?
 */
export const intendedTargetElement = (
    selector: string,
    element: Object
): HTMLElement | intendedTargetElement => {
    if (element && !element.classList.contains(selector)) {
        if (element.parentNode) {
            return intendedTargetElement(selector, element.parentNode);
        }
    }

    return element;
};

export const getFetchParams = (data: Object, method: string = 'get'): Object => {
    let params = {
        credentials: 'same-origin',
        headers: {'content-type': 'application/json'},
        method
    };

    if (data) {
        params = {
            ...params,
            method: 'post',
            body: JSON.stringify(data)
        };
    }

    return params;
};

export const getPrice = (value: number, currency: string = '£', roundBy: number = 100): string => {
    return `${currency}${round(value / roundBy, 2)}`;
};

export const preloadImages = (images: Array<string> = []): void => {
    images.map(imageURL => {
        if (typeof Image !== undefined) {
            let image = new Image();

            image.src = imageURL;
        }
    });
};

export const handleFetchErrors = (response: Object) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
};

export const isMobile = () => {
    if (typeof window !== 'undefined') {
        const layout = window
            .getComputedStyle(document.querySelector('body'), ':before')
            .getPropertyValue('content');

        return layout.includes('mobile');
    }

    return false;
};

export const getOrdinal = i => {
    const j = i % 10;
    const k = i % 100;

    if (j == 1 && k != 11) {
        return `${i}st`;
    }

    if (j == 2 && k != 12) {
        return `${i}nd`;
    }

    if (j == 3 && k != 13) {
        return `${i}rd`;
    }

    return `${i}th`;
};
