/**
 * @prettier
 * @flow
 */
import get from 'lodash/get';
import {getFetchParams} from './services/helpers';
import '../sass/subscription-tool.scss';

let APIKey;

const getUserSubscriptions = () => {
    fetch(
        `https://api.rechargeapps.com/customers/${get(window, 'globalData.customerId')}`,
        getFetchParams({}, 'get', {'X-Recharge-Access-Token': APIKey})
    )
        .then(r => r.json())
        .then(console.log);
};

const init = () => {
    APIKey = get(window, 'globalData.rechargeAPIKey');

    if (APIKey) {
        getUserSubscriptions();
    }
};

if (window.isModernBrowser) {
    window.addEventListener('load', init);
}
