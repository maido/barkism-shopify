/**
 * @prettier
 * @flow
 */
export const SPACING = 24;
export const SPACING_SMALL = 12;
export const SPACING_TINY = 8;

export const ANIMATION_DURATION = 600;
export const ANIMATION_EASING = 'easeOutExpo';
export const ANIMATION_ELASTICITY = ANIMATION_DURATION / 3;

export const SUBSCRIPTION_CHARGE_DAY = 1;
export const SUBSCRIPTION_SHIPPING_DAY = 7;
export const SUBSCRIPTION_CUTOFF_DAY = 20;
export const SUBSCRIPTION_SHIPPING_WORKING_DAYS = 2;
