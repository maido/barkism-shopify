/**
 * @prettier
 * @flow
 */
// import bugsnag from './bugsnag';
import Sticky from 'sticky-js';
import hasJS from './components/has-js';
import overlayNavigation from './components/overlay-navigation';
import modal from './components/modal';
import notifications from './components/notifications';
import scrollMonitor from './components/scroll-monitor';
import tracking from './components/tracking';
import visibilityToggle from './components/visibility-toggle';
import {isMobile} from './services/helpers';
import '../sass/main.scss';

const stickySetup = () => {
    if (typeof window !== 'undefined' && document.body) {
        const meetsThreshold =
            Math.round((document.body.scrollHeight / window.innerHeight) * 100) >= 150;

        if (!isMobile() && meetsThreshold) {
            new Sticky('.js-sticky');
        }
    }
};

const init = () => {
    hasJS.init();
    overlayNavigation.init();
    modal.init();
    notifications.init();
    scrollMonitor.init();
    tracking.init();
    visibilityToggle.init();

    stickySetup();
};

if (window.isModernBrowser) {
    window.addEventListener('load', init);
}
