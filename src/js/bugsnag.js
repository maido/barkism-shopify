/**
 * @prettier
 * @flow
 */
import bugsnag from 'bugsnag-js';

const bugsnagKey = process.env.BUGSNAG_KEY;

if (bugsnagKey == null) {
    throw new Error('Missing APP_GRAPHQL_ENDPOINT');
}

const bugsnagClient = bugsnag(bugsnagKey);
