/**
 * @prettier
 * @flow
 */
import anime from 'animejs';

type OverlayNavigationElements = {
    $body?: HTMLElement,
    $labels?: NodeList<HTMLElement>,
    $links?: NodeList<HTMLLinkElement>,
    $overlay?: HTMLElement,
    $overlayInner?: HTMLElement,
    $siteHeader?: HTMLElement,
    $toggleButton?: HTMLElement
};

const els: OverlayNavigationElements = {};
let isVisible: boolean = false;

const hideOverlay = () => {
    const {$body, $labels, $links, $overlay, $overlayInner, $siteHeader, $toggleButton} = els;

    if ($body && $labels && $links && $overlay && $overlayInner && $siteHeader && $toggleButton) {
        anime({
            begin() {
                if ($body.classList.contains('_s-hero')) {
                    $body.classList.remove('s-no-hero');
                }

                $body.classList.remove('u-hide-body');
                $toggleButton.classList.remove('c-hamburger--active');
            },
            complete() {
                isVisible = false;
                $overlay.style.display = 'none';
            },
            delay: 300,
            duration: 200,
            easing: 'linear',
            opacity: [1, 0],
            targets: $overlay
        });

        const timeline = anime.timeline();

        timeline
            .add({
                duration: 600,
                delay: (target, index) => index * 50,
                elasticity: 50,
                opacity: [1, 0],
                targets: $links,
                translateY: [0, 12]
            })
            .add({
                duration: 600,
                delay: (target, index) => index * 100,
                elasticity: 100,
                offset: '-=900',
                opacity: [1, 0],
                targets: $labels,
                translateY: [0, 6]
            });
    }
};

const showOverlay = () => {
    const {$body, $labels, $links, $overlay, $overlayInner, $siteHeader, $toggleButton} = els;

    if ($body && $labels && $links && $overlay && $overlayInner && $siteHeader && $toggleButton) {
        const timeline = anime.timeline();

        timeline
            .add({
                begin() {
                    isVisible = true;
                    $overlay.style.display = 'flex';

                    if ($body.classList.contains('s-hero')) {
                        $body.classList.add('s-no-hero', '_s-hero');
                    }

                    $body.classList.add('u-hide-body');
                    $toggleButton.classList.add('c-hamburger--active');
                },
                duration: 200,
                easing: 'linear',
                opacity: [0, 1],
                targets: $overlay
            })
            .add({
                duration: 800,
                delay: (target, index) => index * 100,
                elasticity: 100,
                opacity: [0, 1],
                targets: $links,
                translateY: [24, 0]
            })
            .add({
                duration: 800,
                delay: (target, index) => index * 100,
                elasticity: 100,
                offset: '-=1000',
                opacity: [0, 1],
                targets: $labels,
                translateY: [12, 0]
            });
    }
};

const handleHamburgerClick = (event: Event) => {
    const $siteHeader = els.$siteHeader;
    const $toggleButton = els.$toggleButton;

    if ($siteHeader && $toggleButton) {
        if (isVisible === false) {
            showOverlay();
        } else {
            hideOverlay();
        }
    }

    event.preventDefault();
};

const events = () => {
    if (els.$toggleButton) {
        els.$toggleButton.addEventListener('click', handleHamburgerClick, false);
    }
};

const cache = () => {
    els.$body = ((document.querySelector('body'): any): HTMLElement);
    els.$labels = ((document.querySelectorAll('.js-overlay-nav-label'): any): NodeList<
        HTMLElement
    >);
    els.$links = ((document.querySelectorAll('.js-overlay-nav-link'): any): NodeList<
        HTMLLinkElement
    >);
    els.$siteHeader = ((document.querySelector('.js-site-header'): any): HTMLElement);
    els.$overlay = ((document.querySelector('.js-overlay-nav'): any): HTMLElement);
    els.$overlayInner = ((document.querySelector('.js-overlay-nav-inner'): any): HTMLElement);
    els.$toggleButton = ((document.querySelector('.js-overlay-nav-toggle'): any): HTMLElement);
};

const init = () => {
    cache();
    events();
};

export default {init};
