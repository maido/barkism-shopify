/**
 * @prettier
 * @flow
 */
import anime from 'animejs';
import Cookie from 'js-cookie';
import get from 'lodash/get';
import {ANIMATION_DURATION, ANIMATION_EASING, SPACING_SMALL} from '../config';
import {closeModal, init as modalInit, openModal, setActiveModal} from './modal';
import tracking, {track} from './tracking';
import {formatFormDataToParams} from '../services/helpers';

type PromoPopupElements = {
    $closeButtons?: NodeList<HTMLElement>,
    $links?: NodeList<HTMLLinkElement>,
    $primaryCTA?: HTMLElement,
    $secondaryCTA?: HTMLElement,
    $wrapper?: HTMLElement
};

const PROMO_OPTIONS = {
    ...{
        cookieDuration: 365,
        isActive: false,
        modalTimeout: 5000,
        primaryCTAURL: '',
        secondaryCTAURL: '',
        title: ''
    },
    ...get(window, 'globalData.promoPopup', {})
};
const PROMO_MODAL_ID = 'promo-popup';
const PROMO_COOKIE_ID = 'barkism-promo-popup';

const els: PromoPopupElements = {};

const trackAction = (action: string, label: string = '', isNonInteractive: boolean = false) => {
    track('event', {
        ec: `Promo PopUp – ${PROMO_OPTIONS.title}`,
        ea: action,
        el: label,
        ni: isNonInteractive ? 1 : null
    });
};

const getCookie = () => Cookie.get(PROMO_COOKIE_ID);

const setCookie = () => {
    Cookie.set(PROMO_COOKIE_ID, PROMO_OPTIONS.title, {
        expires: parseInt(PROMO_OPTIONS.cookieDuration)
    });
};

const handleModalClose = () => {
    setCookie();
    trackAction('Close');
    closeModal();
};

const showPromotion = () => {
    setActiveModal(PROMO_MODAL_ID);
    openModal(true);
    trackAction('Open', '', true);
};

const handleLinkClick = () => {
    trackAction('Info link click', PROMO_OPTIONS.primaryCTAURL);
    setCookie();
};

const handlePrimaryCTAClick = () => {
    trackAction('Primary CTA click', PROMO_OPTIONS.primaryCTAURL);
    setCookie();
};

const handleSecondaryCTAClick = () => {
    trackAction('Secondary CTA click', PROMO_OPTIONS.secondaryCTAURL);
    setCookie();
};

const hasActiveCookie = () => {
    const cookie = getCookie();

    return cookie && cookie === PROMO_OPTIONS.title;
};

const events = () => {
    if (els.$closeButtons) {
        [...els.$closeButtons].map($button =>
            $button.addEventListener('click', handleModalClose, false)
        );
    }

    if (els.$links) {
        [...els.$links].map($link => $link.addEventListener('click', handleLinkClick, false));
    }

    if (els.$primaryCTA) {
        els.$primaryCTA.addEventListener('click', handlePrimaryCTAClick, false);
    }

    if (els.$secondaryCTA) {
        els.$secondaryCTA.addEventListener('click', handleSecondaryCTAClick, false);
    }
};

const cache = () => {
    els.$closeButtons = ((document.querySelectorAll('.js-modal-promo-popup-close'): any): NodeList<
        HTMLElement
    >);
    els.$links = ((document.querySelectorAll(
        '.js-modal[data-modal="promo-popup"] a:not(.c-button)'
    ): any): NodeList<HTMLLinkElement>);
    els.$primaryCTA = ((document.querySelector('.js-promo-popup-primary-cta'): any): HTMLElement);
    els.$secondaryCTA = ((document.querySelector(
        '.js-promo-popup-secondary-cta'
    ): any): HTMLElement);
};

const init = () => {
    if (PROMO_OPTIONS.isActive && !hasActiveCookie()) {
        tracking.init(false);
        modalInit();
        cache();
        events();
        setTimeout(showPromotion, PROMO_OPTIONS.modalTimeout);
    }
};

export default {init};
