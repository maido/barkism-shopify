/**
 * @prettier
 * @flow
 */
import React, {Fragment} from 'react';

const AccountReferrals = () => (
    <Fragment>
        <h2 className="u-heading u-mb u-mb+@mobile">Your referrals</h2>

        <section className="t-white u-shadow u-p u-p+@mobile">
            <p>Our referral scheme is still being worked on. Please check back soon for updates.</p>
        </section>
    </Fragment>
);

export default AccountReferrals;
