/**
 * @prettier
 * @flow
 */
import React, {Component, Fragment} from 'react';
import find from 'lodash/find';
import map from 'lodash/map';
import get from 'lodash/get';
import smoothscroll from 'smoothscroll-polyfill';
import FeedbackBanner from './components/FeedbackBanner';
import RadioButton from './components/RadioButton';
import {getFetchParams} from '../../services/helpers';

type FormStatus = 'disabled' | 'enabled' | 'posting' | 'error';
type Props = {
    completeAction: Function,
    customerId: number,
    dogInformation: DogInformation,
    notifications: Array<Notification>,
    updateState: Function
};
type State = {
    feedback: Feedback | null,
    fields: DogInformation,
    formStatus: FormStatus
};

const API_URL = get(window, 'globalData.apiURL', '/');

class AccountYourDog extends Component<Props, State> {
    state = {
        feedback: null,
        fields: {
            ...{
                name: '',
                breed: '',
                birthday: '',
                instagram: '',
                fussiness: '',
                toy_destroy: ''
            },
            ...this.props.dogInformation
        },
        formStatus: 'disabled'
    };

    componentDidMount() {
        const notification = find(this.props.notifications, {key: 'dogInformation'});

        if (notification) {
            this.updateFeedback({
                label: notification.label,
                text: notification.text
            });

            if (!this.props.dogInformation.name) {
                this.setDefaultDogDetailsFromOrder();
            }
        }
    }

    updateFeedback = (feedback: Feedback | null, timeout?: number) => {
        this.setState({feedback});

        if (timeout) {
            setTimeout(() => this.updateFeedback(null), timeout);
        }
    };

    setDefaultDogDetailsFromOrder = () => {
        fetch(
            `${API_URL}customers/set-default-dog-information/${this.props.customerId}`,
            getFetchParams(null, 'post')
        )
            .then(r => r.json())
            .then(r => {
                if (r.name && r.breed && !this.state.fields.name && !this.state.fields.breed) {
                    this.setState({
                        fields: {
                            ...this.state.fields,
                            ...{breed: r.breed, name: r.name}
                        },
                        formStatus: 'enabled'
                    });
                }
            });
    };

    updateFieldValue = (key: string, value: string) => {
        this.setState({
            fields: {...this.state.fields, ...{[key]: value}},
            formStatus: 'enabled'
        });
    };

    handleTextChange = (key: string, event: Event) => {
        const $target: HTMLInputElement = (event.target: any);

        if ($target) {
            this.updateFieldValue(key, $target.value);
        }
    };

    handleRadioChange = (key: string, value: string, event: Event) => {
        const $target: HTMLButtonElement = (event.target: any);

        if ($target) {
            $target.blur();
        }

        this.updateFieldValue(key, value);
    };

    getChangedValues = () => {
        let values = [];

        map(this.state.fields, (value, key) => {
            if (!this.props.dogInformation[key] || this.props.dogInformation[key] !== value) {
                values.push({key, value});
            }
        });

        return values;
    };

    handleUpdateError = () => {
        this.setState({formStatus: 'enabled'});
        this.updateFeedback({
            label: 'Error!',
            text: `We couldn't save your changes. Please try again and if the error persists, please get in touch.`,
            theme: 'error'
        });

        if (typeof window !== 'undefined') {
            window.scrollTo({top: 0, behavior: 'smooth'});
        }
    };

    handleUpdateSuccess = () => {
        this.setState({formStatus: 'enabled'});
        this.updateFeedback(
            {
                label: 'Success!',
                text: `${
                    this.state.fields.name ? `${this.state.fields.name}'s` : "Your dog's"
                } details were saved`,
                theme: 'success'
            },
            3000
        );
        this.props.completeAction('dogInformation');
        this.props.updateState('dogInformation', this.state.fields);

        if (typeof window !== 'undefined') {
            window.scrollTo({top: 0, behavior: 'smooth'});
        }
    };

    submitForm = (changedValues: Array<Object>) => {
        const changedValuesMetafields = changedValues.map(value => {
            return {
                owner_resource: 'customer',
                owner_id: this.props.customerId,
                namespace: 'dogdetails',
                key: value.key,
                value: value.value,
                value_type: 'string'
            };
        });

        changedValuesMetafields.push({
            owner_resource: 'customer',
            owner_id: this.props.customerId,
            namespace: 'actionscompleted',
            key: 'dogdetails',
            value: new Date().getTime(),
            value_type: 'integer'
        });

        const body = {metafields: changedValuesMetafields};

        fetch(`${API_URL}metafields`, getFetchParams(body))
            .then(r => r.json())
            .then(r => {
                if (r.errors) {
                    this.handleUpdateError();
                } else {
                    this.handleUpdateSuccess();
                }
            })
            .catch(this.handleUpdateError);
    };

    handleFormSubmit = (event: Event | null) => {
        if (event) {
            event.preventDefault();
        }

        this.setState({formStatus: 'posting'});

        const changedValues = this.getChangedValues();

        if (changedValues.length) {
            this.submitForm(changedValues);
        } else {
            setTimeout(() => this.handleUpdateSuccess(), 500);
        }
    };

    render() {
        const {feedback, formStatus} = this.state;
        const {name, breed, birthday, instagram, fussiness, toy_destroy} = this.state.fields;

        return (
            <Fragment>
                <h2 className="u-heading u-mb u-mb+@mobile">
                    {name ? name : 'Your dog'}'s information
                </h2>

                <section className="t-white u-shadow u-p u-p+@mobile">
                    {feedback ? (
                        <div className="c-animated-content u-mb">
                            <FeedbackBanner {...feedback} />
                        </div>
                    ) : null}

                    <form className="c-form" onSubmit={this.handleFormSubmit}>
                        <div className="u-flex@mobile">
                            <div className="c-form__field u-6/12@mobile">
                                <label className="u-label u-display-block u-mb--" htmlFor="name">
                                    Name
                                </label>
                                <input
                                    id="name"
                                    type="text"
                                    className="c-form__input u-1/1 u-p-"
                                    maxLength="30"
                                    placeholder="Chip"
                                    name="name"
                                    onChange={this.handleTextChange.bind(this, 'name')}
                                    value={name}
                                />
                            </div>
                            <div className="c-form__field u-6/12@mobile u-mt- u-mt0@mobile u-ml-@mobile">
                                <label className="u-label u-display-block u-mb--" htmlFor="breed">
                                    Breed
                                </label>
                                <input
                                    id="breed"
                                    type="text"
                                    className="c-form__input u-1/1 u-p-"
                                    maxLength="30"
                                    placeholder="Dachshund"
                                    name="breed"
                                    onChange={this.handleTextChange.bind(this, 'breed')}
                                    value={breed}
                                />
                            </div>
                        </div>

                        <div className="u-flex@mobile u-mt">
                            <div className="c-form__field u-6/12@mobile">
                                <label
                                    className="u-label u-display-block u-mb--"
                                    htmlFor="birthday"
                                >
                                    Birthday
                                </label>
                                <input
                                    id="birthday"
                                    type="date"
                                    className="c-form__input u-1/1 u-p-"
                                    maxLength="30"
                                    placeholder="dd / mm / yyyy"
                                    name="birthday"
                                    onChange={this.handleTextChange.bind(this, 'birthday')}
                                    value={birthday}
                                />
                            </div>
                            <div className="c-form__field u-6/12@mobile u-mt- u-mt0@mobile u-ml-@mobile">
                                <label
                                    className="u-label u-display-block u-mb--"
                                    htmlFor="instagram"
                                >
                                    Instagram profile
                                </label>
                                <div className="c-form__input-username">
                                    <input
                                        id="instagram"
                                        type="text"
                                        className="c-form__input u-1/1 u-p-"
                                        maxLength="30"
                                        placeholder="chipthedaxi"
                                        name="instagram"
                                        onChange={this.handleTextChange.bind(this, 'instagram')}
                                        value={instagram}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="c-form__field u-mt">
                            <label className="u-label u-display-block u-mb--" htmlFor="fussiness">
                                {name ? `How fussy is ${name}?` : 'How fussy?'}
                            </label>
                            <textarea
                                id="fussiness"
                                className="c-form__input c-form__input--multiline u-1/1 u-p-"
                                placeholder="Will only eat roast chicken (and gravy, of course)"
                                name="fussiness"
                                style={{height: 86}}
                                onChange={this.handleTextChange.bind(this, 'fussiness')}
                                value={fussiness}
                            />
                        </div>

                        <div className="c-form__field u-mt">
                            <label className="u-label u-display-block u-mb--" htmlFor="toy_destroy">
                                Toy destroy speed?
                            </label>

                            <div className="c-button-list c-button-list--flush c-button-list--small">
                                <RadioButton
                                    classes="u-border-1 u-pv- u-4/12"
                                    onClick={this.handleRadioChange.bind(
                                        this,
                                        'toy_destroy',
                                        'Minutes'
                                    )}
                                    isSelected={toy_destroy === 'Minutes'}
                                >
                                    Minutes
                                </RadioButton>
                                <RadioButton
                                    classes="u-border-1 u-pv- u-4/12"
                                    onClick={this.handleRadioChange.bind(
                                        this,
                                        'toy_destroy',
                                        'Hours'
                                    )}
                                    isSelected={toy_destroy === 'Hours'}
                                >
                                    Hours
                                </RadioButton>
                                <RadioButton
                                    classes="u-border-1 u-pv- u-4/12"
                                    onClick={this.handleRadioChange.bind(
                                        this,
                                        'toy_destroy',
                                        'Never'
                                    )}
                                    isSelected={toy_destroy === 'Never'}
                                >
                                    Never
                                </RadioButton>
                            </div>
                        </div>

                        <div className="u-mt">
                            <button
                                type="submit"
                                className="c-button c-button--primary"
                                disabled={formStatus !== 'enabled'}
                                style={{
                                    opacity: formStatus === 'enabled' ? 1 : 0.5
                                }}
                            >
                                {formStatus === 'posting' ? 'Updating' : 'Update'} details
                            </button>
                        </div>
                    </form>
                </section>
            </Fragment>
        );
    }
}

export default AccountYourDog;
