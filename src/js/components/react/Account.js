/**
 * @prettier
 * @flow
 */
import React, {Component} from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import get from 'lodash/get';
import reject from 'lodash/reject';
import notifications, {clearNotification, readNotifications} from '../notifications';
import AccountHome from './AccountHome';
import AccountYourDog from './AccountYourDog';
import AccountReferrals from './AccountReferrals';
import AccountSubscription from './AccountSubscription';
import SelectNavigation from './components/SelectNavigation';
import SidebarNavigation from './components/SidebarNavigation';
import '../../../sass/account.scss';

type Props = {
    customerId: number,
    dogInformation: DogInformation,
    subscriptionManagementURL: string,
    subscriptionStatus: string
};
type State = {
    dogInformation: DogInformation,
    notifications: Array<Notification>,
    navLinks: Array<{
        key?: string,
        path: string,
        label: string
    }>
};

const API_URL = get(window, 'globalData.apiURL', '/');
const ROOT_URL = get(window, 'globalData.rootURL', '/');

class Account extends Component<Props, State> {
    state = {
        dogInformation: this.props.dogInformation,
        notifications: readNotifications(),
        navLinks: [
            {path: '/your-dog', label: 'Dog information', key: 'dogInformation'},
            {path: '/subscription', label: 'Subscriptions', key: 'subscription'},
            {path: `${ROOT_URL}/pages/contact`, label: 'Help'}
        ]
    };

    componentDidMount() {
        /**
         * Prevent cold requests from taking too long to respond.
         * Need to think of a workaround for this...
         */
        fetch(API_URL);
        notifications.init();
    }

    completeAction = (key: string) => {
        const updatedNotifications = clearNotification(key);

        this.setState({notifications: updatedNotifications});
    };

    updateState = (key: string, value: string | number | Array<*> | Object) => {
        /**
         * This is a workaround until we introuce better global state handling. When the route is rendered
         * it uses the data passed from the global data set from the account template so we just need to
         * update that with the latest data added by the user.
         */
        this.setState({[key]: value});
    };

    render() {
        const {customerId, subscriptionManagementURL, subscriptionStatus} = this.props;
        const {dogInformation, notifications, navLinks} = this.state;

        return (
            <div className="o-layout u-stack-layout@mobile">
                <div className="o-layout__item u-4/12@mobile u-3/12@desktop">
                    <div className="u-show@mobile">
                        <SidebarNavigation notifications={notifications} links={navLinks} />
                    </div>
                    <div className="u-hide@mobile">
                        <SelectNavigation links={navLinks} />
                    </div>
                </div>
                <div className="o-layout__item u-8/12@mobile u-9/12@desktop">
                    <Switch>
                        <Route exact path="/" render={() => <Redirect to="/your-dog" />} />
                        <Route
                            path="/your-dog"
                            render={() => (
                                <AccountYourDog
                                    notifications={notifications}
                                    completeAction={this.completeAction}
                                    customerId={customerId}
                                    dogInformation={dogInformation}
                                    updateState={this.updateState}
                                />
                            )}
                        />
                        <Route
                            exact
                            path="/subscription"
                            render={() => (
                                <AccountSubscription
                                    manageURL={subscriptionManagementURL}
                                    status={subscriptionStatus}
                                />
                            )}
                        />
                        <Route exact path="/referrals" component={AccountReferrals} />
                    </Switch>
                </div>
            </div>
        );
    }
}

export default Account;
