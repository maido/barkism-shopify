/**
 * @prettier
 * @flow
 */
import React, {Component} from 'react';
import get from 'lodash/get';
import has from 'lodash/has';
import smoothscroll from 'smoothscroll-polyfill';
import {getFetchParams, getPrice, handleFetchErrors, isMobile} from '../../services/helpers';
import {closeModal, init as modalInit, openModal, setActiveModal} from '../modal';
import tracking, {track} from '../tracking';
import RadioButton from './components/RadioButton';
import Label from './components/Label';
import SubscriptionFormStep from './components/SubscriptionFormStep';
import SubscriptionOrderReview from './components/SubscriptionOrderReview';
import SubscriptionReviewBanner from './components/SubscriptionReviewBanner';

type Props = {
    allProducts: {
        gift1: ShopifyProduct,
        gift2: ShopifyProduct,
        gift4: ShopifyProduct,
        subscription: ShopifyProduct
    },
    copy: Object,
    images: Object,
    manageURL: string,
    product: ShopifyProduct,
    subscriptionProducts: Object,
    userStatus: string
};
type State = {
    defaultPrice: number,
    dogBreed: string,
    dogName: string,
    dogSize: string,
    duration: string,
    formComplete: boolean,
    formSubmitted: boolean,
    hasReviewedGiftShipping: boolean,
    message: string,
    productVariant: ShopifyProductVariant | null,
    selectedProductId: number | null,
    showManageSubscriptionBanner: boolean,
    steps: {
        completed: number,
        current: number,
        previous: number
    },
    type: string
};

const ROOT_URL = get(window, 'globalData.rootURL', '');

const emptyCart = () => {
    fetch(`${ROOT_URL}/cart/clear.js`, getFetchParams({}, 'post'))
        .then(handleFetchErrors)
        .catch(handleFetchErrors);
};

class Subscription extends Component<Props, State> {
    constructor(props?: Props) {
        super(props);

        const defaultPrice = get(this.props.product, 'variants[0].price', 2500);
        const type = get(this.props.product, 'type', 'subscription');

        this.state = {
            defaultPrice,
            dogBreed: '',
            dogName: '',
            dogSize: '',
            duration: '',
            formComplete: false,
            formSubmitted: false,
            hasReviewedGiftShipping: false,
            message: '',
            productVariant: null,
            selectedProductId: null,
            steps: this.getDefaultSteps(),
            showManageSubscriptionBanner: this.props.userStatus !== 'none',
            type
        };
    }

    componentDidMount() {
        emptyCart();
        modalInit();
        tracking.init(false);
        smoothscroll.polyfill();
    }

    getDefaultSteps() {
        /**
         * If we don't need to show a message to current subscribers we can set the current step
         * to 2, as the first step will already have its answer selected based on the current product type
         */
        if (this.props.userStatus === 'none') {
            return {
                completed: 1,
                current: 2,
                previous: 1
            };
        }

        return {
            completed: 0,
            current: 0,
            previous: 0
        };
    }

    scrollToCurrentStep() {
        const {current, previous} = this.state.steps;

        /**
         * Ignore moving the window position for the dog name as it has another input field right next to it
         */
        if (current !== 4 && current !== 6 && current !== previous) {
            let top;

            if (current > previous && current >= 2) {
                top = isMobile ? +300 : +200;
            } else {
                top = isMobile ? -300 : -200;
            }

            window.scrollBy({top, behavior: 'smooth'});
        }
    }

    isFormComplete() {
        const {dogBreed, dogName, steps, type} = this.state;

        if (steps.completed >= 3 && dogBreed !== '' && dogName !== '') {
            if (type === 'gift') {
                return steps.completed >= 5;
            } else {
                return true;
            }
        }

        return false;
    }

    getProductKey() {
        const productKey =
            this.state.type === 'gift' && this.state.steps.completed >= 5
                ? `gift${this.state.duration}`
                : 'subscription';

        return productKey;
    }

    getProductId() {
        const productKey = this.getProductKey();

        if (this.props.allProducts.hasOwnProperty(productKey)) {
            const productId = this.props.allProducts[productKey].id;

            return productId;
        }
    }

    getProduct() {
        if (this.state.dogSize) {
            const productKey = this.getProductKey();
            const productVariant = this.props.allProducts[productKey].variants
                .filter(v => v.title === this.state.dogSize)
                .reduce(v => v);

            if (productVariant) {
                return productVariant;
            }
        }

        return null;
    }

    setupReviewModal() {
        const $dogNames = document.querySelectorAll(
            '[data-modal="gift-subscription-review"] .js-dog-name'
        );
        const $submitCTA = document.querySelector(
            '[data-modal="gift-subscription-review"] .js-cta'
        );

        if ($dogNames) {
            [...$dogNames].map($dogName => ($dogName.innerText = this.state.dogName));
        }

        if ($submitCTA) {
            $submitCTA.addEventListener('click', this.handleFormSubmit.bind(this), false);
        }
    }

    handleContinueWithSubscription() {
        this.setState({
            showManageSubscriptionBanner: false,
            steps: {
                completed: 1,
                current: 2,
                previous: 1
            }
        });
    }

    handleAnswerChange(key: string, value: number | string) {
        const nextSteps = {
            type: 2,
            dogSize: 3,
            dogName: 4,
            dogBreed: 4,
            duration: 5,
            message: 6
        };
        const nextStep = nextSteps[key];
        const steps = {
            completed:
                this.state.steps.completed > nextStep ? this.state.steps.completed : nextStep,
            current: nextStep,
            previous: this.state.steps.current
        };

        // if (nextStep !== this.state.steps.current) {
        // track('event', {
        //     ec: `Question ${this.state.steps.current} answered`,
        //     el: `${this.state.type} form`
        // });
        // }

        this.setState({[key]: value, steps}, () => {
            this.setState({
                formComplete: this.isFormComplete(),
                selectedProductId: this.getProductId(),
                productVariant: this.getProduct()
            });
            this.scrollToCurrentStep();
        });
    }

    handleRadioChange(key: string, value: number | string, event: Event) {
        const $target: HTMLButtonElement = (event.target: any);

        if ($target) {
            $target.blur();
        }

        this.handleAnswerChange(key, value);
    }

    handleTextChange(key: string, event: Event) {
        const $target: HTMLInputElement = (event.target: any);

        if ($target) {
            this.handleAnswerChange(key, $target.value);
        }
    }

    getCartAddData(variantID: number) {
        const rechargeData = get(window, 'ReCharge.products[0]');

        if (rechargeData) {
            const data = {
                id: variantID,
                properties: {
                    dog_name: this.state.dogName,
                    dog_breed: this.state.dogBreed,
                    message: this.state.message,
                    shipping_interval_frequency: rechargeData.shipping_interval_frequency[0],
                    shipping_interval_unit_type: rechargeData.shipping_interval_unit_type,
                    subscription_id: rechargeData.subscription_id
                },
                quantity: 1
            };

            return data;
        } else {
            alert(
                'Sorry – something went wrong with the subscription data. Please refresh and try again. If this persists, please contact hello@barkism.com.'
            );
        }
    }

    updateRechargeSubscriptionInformation() {
        if (this.state.selectedProductId) {
            const subscriptionProduct = this.props.subscriptionProducts[
                this.state.selectedProductId
            ];

            if (subscriptionProduct) {
                const keys = Object.keys(subscriptionProduct);

                /**
                 * ReCharge's information is stored outside of the React element and doesn't update dynamically so we need
                 * to update the fields manually.
                 */
                keys.map(key => {
                    const $field: HTMLInputElement = (document.querySelector(`#${key}`): any);

                    if ($field) {
                        $field.setAttribute('value', subscriptionProduct[key]);
                    }
                });

                return subscriptionProduct.rc_subscription_id;
            } else {
                alert(
                    "Sorry, we couldn't find the subscription information. Please refresh and try again. If this persists please get in touch with us at hello@barksim.com."
                );
            }
        }
    }

    handleFormSubmit(event: Event | null) {
        if (event) {
            event.preventDefault();
        }

        if (this.state.type === 'gift') {
            if (!this.state.hasReviewedGiftShipping) {
                this.setState({hasReviewedGiftShipping: true});
                this.setupReviewModal();

                track('event', {ec: 'Gift address modal - Review ', el: 'Subscribe form'});
                setActiveModal('gift-subscription-review');
                openModal(true);

                return;
            } else {
                track('event', {ec: 'Gift address modal - Continue ', el: 'Subscribe form'});
            }
        }

        const subscriptionId = this.updateRechargeSubscriptionInformation();
        const variantID = this.state.productVariant ? this.state.productVariant.id : null;

        if (this.state.formComplete && variantID) {
            fetch(`${ROOT_URL}/cart/add.js`, getFetchParams(this.getCartAddData(variantID), 'post'))
                .then(response => {
                    if (response.status >= 400) {
                        throw new Error('Bad response from server');
                    }
                    return response.json();
                })
                .then(response => {
                    if (response && response.variant_id === variantID) {
                        if (window.reChargeProcessCart) {
                            track('event', {
                                ec: 'Subscription added to cart',
                                el: `${this.state.type} form`
                            });

                            window.location = window.reChargeProcessCart(subscriptionId);
                        } else {
                            alert(
                                'Sorry – there was an error creating the subscription. Please refresh and try again. If this persists, please contact hello@barkism.com.'
                            );

                            throw new Error("ReCharge's process function was not defined");
                        }
                    } else {
                        alert(
                            'Sorry – Unknown error. Please refresh and try again. If this persists, please contact hello@barkism.com.'
                        );

                        throw new Error('The wrong product was added to cart');
                    }
                })
                .catch(console.error);

            this.setState({formSubmitted: true});
        }
    }

    showBigSizeFallback() {
        track('event', {ec: 'Big box form modal', el: 'Subscribe form'});
        setActiveModal('box-sizes-fallback');
        openModal(true);
    }

    showDogSizesGuide() {
        track('event', {ec: 'Dog sizes guide modal', el: 'Subscribe form'});
        setActiveModal('dog-sizes-guide');
        openModal(true);
    }

    render() {
        const {allProducts, copy, images, manageURL, product, userStatus} = this.props;
        const {
            defaultPrice,
            dogBreed,
            dogName,
            dogSize,
            duration,
            formComplete,
            formSubmitted,
            message,
            productVariant,
            showManageSubscriptionBanner,
            steps,
            type
        } = this.state;

        return (
            <form
                autoComplete="off"
                method="post"
                encType="multipart/form-data"
                className="c-form c-form--blocks"
                onSubmit={this.handleFormSubmit.bind(this)}
            >
                <h1 className="u-heading u-mb u-mb+@mobile" itemProp="name">
                    {type === 'gift' ? 'Send a gift' : 'Sign up'}
                </h1>

                {showManageSubscriptionBanner ? (
                    <div className="u-mb u-mb+@mobile">
                        <SubscriptionReviewBanner
                            handleContinue={this.handleContinueWithSubscription.bind(this)}
                            manageSubscriptionURL={manageURL}
                            subscriptionStatus={userStatus}
                        />
                    </div>
                ) : null}

                <SubscriptionFormStep isActive={steps.current >= 1}>
                    {isActive => (
                        <div>
                            <Label>1. Who is it for?</Label>

                            <div className="c-button-list c-button-list--tall">
                                <RadioButton
                                    classes="c-button--shadow u-6/12"
                                    onClick={this.handleRadioChange.bind(
                                        this,
                                        'type',
                                        'subscription'
                                    )}
                                    isSelected={type === 'subscription'}
                                >
                                    It's for me
                                </RadioButton>
                                <RadioButton
                                    classes="c-button--shadow u-6/12 u-ml-"
                                    onClick={this.handleRadioChange.bind(this, 'type', 'gift')}
                                    isSelected={type === 'gift'}
                                >
                                    It's a gift
                                </RadioButton>
                            </div>
                        </div>
                    )}
                </SubscriptionFormStep>

                <SubscriptionFormStep isActive={steps.completed >= 1}>
                    {isActive => (
                        <div>
                            <button
                                type="button"
                                className="u-pointer u-float-right u-h6 u-font-size-11 u-case-upper u-color-primary"
                                onClick={this.showDogSizesGuide.bind(this)}
                            >
                                Size guide
                            </button>
                            <Label>2. How big is the dog?</Label>

                            <div className="c-button-list c-button-list--tall">
                                <RadioButton
                                    classes="c-button--shadow u-6/12"
                                    onClick={this.handleRadioChange.bind(this, 'dogSize', 'Small')}
                                    isDisabled={!isActive}
                                    isSelected={dogSize === 'Small'}
                                    label="0-20kg"
                                >
                                    Small
                                </RadioButton>
                                <RadioButton
                                    classes="c-button--shadow u-6/12 u-ml-"
                                    onClick={this.showBigSizeFallback.bind(this)}
                                    isDisabled={!isActive}
                                    isSelected={dogSize === 'Big'}
                                    label="20-40kg"
                                >
                                    Big
                                </RadioButton>
                            </div>
                            <input type="hidden" name="" value={dogSize} />
                        </div>
                    )}
                </SubscriptionFormStep>

                <SubscriptionFormStep isActive={steps.completed >= 2}>
                    {isActive => (
                        <div>
                            <Label>3. Who is the lucky dog?</Label>

                            <div className="c-form__field-group">
                                <div className="c-form__field u-6/12@mobile">
                                    <label className="c-form__label" htmlFor="dogName">
                                        Dog's name
                                    </label>
                                    <input
                                        disabled={!isActive}
                                        id="dogName"
                                        type="text"
                                        className="c-form__input"
                                        maxLength="30"
                                        placeholder="Chip"
                                        name="properties[Pet name]"
                                        onChange={this.handleTextChange.bind(this, 'dogName')}
                                        style={{height: 'auto'}}
                                    />
                                </div>
                                <div className="c-form__field u-6/12@mobile">
                                    <label className="c-form__label" htmlFor="dogBreed">
                                        Dog's breed
                                    </label>
                                    <input
                                        disabled={!isActive}
                                        id="dogBreed"
                                        type="text"
                                        className="c-form__input"
                                        maxLength="30"
                                        placeholder="Dachshund"
                                        name="properties[Pet breed]"
                                        onChange={this.handleTextChange.bind(this, 'dogBreed')}
                                        style={{height: 'auto'}}
                                    />
                                </div>
                            </div>
                            <input type="hidden" name="" value={dogName} />
                        </div>
                    )}
                </SubscriptionFormStep>

                <SubscriptionFormStep isActive={steps.completed >= 3} isVisible={type === 'gift'}>
                    {isActive => (
                        <div>
                            <Label>4. Select gift duration</Label>

                            <div className="c-button-list c-button-list--tall">
                                <RadioButton
                                    classes="c-button--shadow u-4/12"
                                    onClick={this.handleRadioChange.bind(this, 'duration', 1)}
                                    isDisabled={!isActive}
                                    isSelected={duration === 1}
                                    label="1 Month"
                                >
                                    <span className="u-h4">
                                        {getPrice(get(allProducts, 'gift1.price'))}
                                    </span>
                                </RadioButton>
                                <RadioButton
                                    classes="c-button--shadow u-4/12 u-ml-"
                                    onClick={this.handleRadioChange.bind(this, 'duration', 2)}
                                    isDisabled={!isActive}
                                    isSelected={duration === 2}
                                    label="2 Months"
                                >
                                    <span className="u-h4">
                                        {getPrice(get(allProducts, 'gift2.price'))}
                                    </span>
                                </RadioButton>
                                <RadioButton
                                    classes="c-button--shadow u-4/12 u-ml-"
                                    onClick={this.handleRadioChange.bind(this, 'duration', 4)}
                                    isDisabled={!isActive}
                                    isSelected={duration === 4}
                                    label="4 Months"
                                >
                                    <span className="u-h4">
                                        {getPrice(get(allProducts, 'gift4.price'))}
                                    </span>
                                </RadioButton>
                            </div>

                            <input type="hidden" name="" value={duration} />
                        </div>
                    )}
                </SubscriptionFormStep>

                <SubscriptionFormStep isActive={steps.completed >= 4} isVisible={type === 'gift'}>
                    {isActive => (
                        <div>
                            <Label>
                                5. Add a personal message{' '}
                                <span className="u-color-grey">(optional)</span>
                            </Label>

                            <div className="c-form__field">
                                <textarea
                                    disabled={!isActive}
                                    className="c-form__input c-form__input--multiline u-1/1"
                                    placeholder="Write your message"
                                    name="properties[Message]"
                                    onChange={this.handleTextChange.bind(this, 'message')}
                                    style={{height: 80}}
                                />
                            </div>
                            <input type="hidden" name="" value={message} />
                        </div>
                    )}
                </SubscriptionFormStep>

                <SubscriptionOrderReview
                    defaultPrice={defaultPrice}
                    formSubmitted={formSubmitted}
                    formDisabled={!formComplete || formSubmitted}
                    product={productVariant}
                    copy={copy}
                    images={images}
                    type={type}
                />
            </form>
        );
    }
}

export default Subscription;
