/**
 * @prettier
 * @flow
 */
import * as React from 'react';

type Props = {
    children: React.Node
};

const Label = ({children}: Props) => (
    <label className="u-label u-display-block u-mb-">{children}</label>
);

export default Label;
