/**
 * @prettier
 * @flow
 */
import * as React from 'react';

type Props = {
    children: React.Node,
    classes?: string,
    isDisabled?: boolean,
    isSelected?: boolean,
    label?: string,
    onClick?: Function
};

const RadioButton = ({
    children,
    classes = '',
    isDisabled = false,
    isSelected = false,
    label,
    onClick
}: Props) => (
    <button
        type="button"
        className={`c-button c-button--white c-button--radio c-button--round-small u-h5 u-ph ${classes} ${
            isSelected ? 'c-button--selected' : ''
        }`}
        disabled={isDisabled}
        onClick={onClick}
        style={{overflow: 'hidden'}}
    >
        {children}
        {label ? <div className="u-label u-color-grey-dark u-mt--">{label}</div> : null}

        <div
            className={`c-button__selected-icon ${
                isSelected ? 'c-button__selected-icon--active' : ''
            }`}
        >
            <svg width="10" height="10" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M.084 5.426a.484.484 0 0 1-.116-.298c0-.085.038-.213.116-.298l.542-.596a.351.351 0 0 1 .542 0l.038.042 2.13 2.512a.176.176 0 0 0 .27 0L8.794.87h.038a.351.351 0 0 1 .542 0l.542.596a.44.44 0 0 1 0 .596L3.723 9.13a.337.337 0 0 1-.271.128.337.337 0 0 1-.271-.128L.16 5.554l-.077-.128z"
                    fill="#FFF"
                    fillRule="nonzero"
                />
            </svg>
        </div>
    </button>
);

export default RadioButton;
