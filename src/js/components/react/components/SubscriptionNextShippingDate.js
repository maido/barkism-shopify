/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import {
    SUBSCRIPTION_SHIPPING_DAY,
    SUBSCRIPTION_CUTOFF_DAY,
    SUBSCRIPTION_SHIPPING_WORKING_DAYS
} from '../../../config';
import {getOrdinal} from '../../../services/helpers';

const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];

const getShippingMonth = (currentMonth: number, currentDay: number) => {
    let month = currentMonth;

    if (currentDay >= SUBSCRIPTION_CUTOFF_DAY) {
        if (month + 1 === 12) {
            month = 0;
        } else {
            month = month + 1;
        }
    }

    return month;
};

const getShippingNotice = () => {
    const date = new Date();
    const currentDay = date.getDate();
    const month = getShippingMonth(date.getMonth(), currentDay);
    let notice = '';

    if (currentDay < SUBSCRIPTION_SHIPPING_DAY) {
        notice = `Your Barkism box will ship on ${months[month]} ${getOrdinal(
            SUBSCRIPTION_SHIPPING_DAY
        )}.`;
    } else if (currentDay >= SUBSCRIPTION_SHIPPING_DAY && currentDay < SUBSCRIPTION_CUTOFF_DAY) {
        notice = `Your Barkism box will ship within ${SUBSCRIPTION_SHIPPING_WORKING_DAYS} working days.`;
    } else if (currentDay >= SUBSCRIPTION_CUTOFF_DAY) {
        notice = `Your first Barkism box will ship on ${months[month]} ${getOrdinal(
            SUBSCRIPTION_SHIPPING_DAY
        )}.`;
    }

    return notice;
};

const SubscriptionNextShippingDate = () => <span>{getShippingNotice()}</span>;

export default SubscriptionNextShippingDate;
