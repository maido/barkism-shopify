/**
 * @prettier
 * @flow
 */
import * as React from 'react';

type Props = {
    label: string,
    text: string,
    theme: string
};

const FeedbackBanner = ({label, text, theme = 'notice'}: Props) => (
    <div className={`c-feedback c-feedback--${theme}`} data-order="1">
        <span className="c-feedback-icon" />
        <span className="c-feedback__label">{label}</span> {text}
    </div>
);

export default FeedbackBanner;
