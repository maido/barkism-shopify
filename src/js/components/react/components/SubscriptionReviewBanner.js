/**
 * @prettier
 * @flow
 */
import React from 'react';

type Props = {
    handleContinue: Function,
    manageSubscriptionURL: string,
    subscriptionStatus: string
};

const SubscriptionReviewBanner = ({
    handleContinue,
    manageSubscriptionURL,
    subscriptionStatus
}: Props) => (
    <div className="u-p t-tertiary u-font-size-14">
        <div className="u-flex">
            <svg
                width="33"
                height="33"
                viewBox="0 0 33 33"
                className="u-mr"
                style={{flex: '0 0 33px'}}
                xmlns="http://www.w3.org/2000/svg"
            >
                <g fill="none" fillRule="evenodd">
                    <circle stroke="#221F20" cx="16.5" cy="16.5" r="16" />
                    <path
                        d="M15.633 20.867c0-.344.112-.627.336-.851.229-.23.52-.344.875-.344.354 0 .643.114.867.344.23.224.344.507.344.851s-.115.63-.344.86c-.224.224-.513.335-.867.335s-.646-.111-.875-.335a1.18 1.18 0 0 1-.336-.86zm.078-10.07h2.273l-.43 7.664h-1.413l-.43-7.664z"
                        fill="#221F20"
                    />
                </g>
            </svg>
            <div>
                <p>
                    <strong>Please note:</strong> You are already have an {subscriptionStatus}{' '}
                    Barkism subscription. You can manage your existing subscription or create an
                    additional one (at an additional charge).
                </p>
                <div className="c-button-list@mobile u-mt">
                    <a
                        href={manageSubscriptionURL}
                        className="c-button c-button--primary c-button--ghost u-block"
                    >
                        Manage existing
                    </a>
                    <button
                        type="button"
                        onClick={handleContinue}
                        className="c-button c-button--primary js-continue"
                    >
                        Add another
                    </button>
                </div>
            </div>
        </div>
    </div>
);

export default SubscriptionReviewBanner;
