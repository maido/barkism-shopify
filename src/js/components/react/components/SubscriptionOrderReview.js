/**
 * @prettier
 * @flow
 */
import React, {Component} from 'react';
import get from 'lodash/get';
import SubscriptionNextShippingDate from './SubscriptionNextShippingDate';
import {getPrice} from '../../../services/helpers';

type Props = {
    copy: {
        renewalLabel: string,
        renewal: string
    },
    defaultPrice: number,
    formSubmitted: boolean,
    formDisabled: boolean,
    images: {
        cards: string
    },
    product: ShopifyProductVariant | null,
    type: string
};

type State = {
    isRenewalCopyVisible: boolean
};

class SubscriptionOrderReview extends Component<Props, State> {
    constructor(props?: Props) {
        super(props);

        this.state = {isRenewalCopyVisible: false};
    }

    toggleRenewalCopyVisible() {
        this.setState({isRenewalCopyVisible: !this.state.isRenewalCopyVisible});
    }

    getFormSubmitLabel() {
        if (this.props.formSubmitted) {
            return 'Please wait...';
        } else {
            return this.props.type === 'subscription' ? 'Continue to payment' : 'Review order';
        }
    }

    render() {
        const {copy, defaultPrice, formSubmitted, formDisabled, images, product, type} = this.props;
        const {isRenewalCopyVisible} = this.state;

        return (
            <div>
                <div className="t-tertiary u-mt+ u-p- u-pv@mobile u-ph+@mobile u-text-align-center u-radius-small">
                    <div className="u-h3 u-mb-- u-font-weight-600">
                        {type === 'gift' ? (
                            <span className="js-price">
                                {getPrice(get(product, 'price', defaultPrice))}
                            </span>
                        ) : (
                            <span className="js-price">
                                {getPrice(get(product, 'price', defaultPrice))}/month
                            </span>
                        )}
                    </div>

                    <div className="u-font-size-12 u-mb-">
                        <div className="u-case-upper u-flex u-justify-center u-align-items-center">
                            <SubscriptionNextShippingDate />

                            <button
                                type="button"
                                onClick={this.toggleRenewalCopyVisible.bind(this)}
                                className={`js-info-toggle ${
                                    type !== 'subscription' ? 'u-hide' : ''
                                }`}
                            >
                                <svg
                                    width="33"
                                    height="33"
                                    viewBox="0 0 33 33"
                                    className="c-svg c-svg--w18"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <g fill="none" fillRule="evenodd">
                                        <circle stroke="#221F20" cx="16.5" cy="16.5" r="16" />
                                        <path
                                            d="M15.633 20.867c0-.344.112-.627.336-.851.229-.23.52-.344.875-.344.354 0 .643.114.867.344.23.224.344.507.344.851s-.115.63-.344.86c-.224.224-.513.335-.867.335s-.646-.111-.875-.335a1.18 1.18 0 0 1-.336-.86zm.078-10.07h2.273l-.43 7.664h-1.413l-.43-7.664z"
                                            fill="#221F20"
                                        />
                                    </g>
                                </svg>
                            </button>
                        </div>

                        {isRenewalCopyVisible ? (
                            <div className="u-mt- u-ph@mobile u-fade-in u-text-align-center js-subscription-renew">
                                {get(copy, 'renewal')}
                            </div>
                        ) : null}
                    </div>

                    <button
                        disabled={formDisabled || formSubmitted || !product}
                        type="submit"
                        name="add"
                        className="c-button c-button--primary c-button--block u-1/1"
                        style={{opacity: formDisabled || formSubmitted ? 0.5 : 1}}
                    >
                        {this.getFormSubmitLabel()}
                    </button>

                    <div className="u-mt- u-font-size-12">
                        <strong>Got a discount code?</strong> You can apply it at the next step
                    </div>

                    <input type="hidden" name="id" value={get(product, 'id', '')} />
                </div>
                <div className="u-mt u-text-align-center">
                    <div className="u-flex u-align-items-center u-justify-center u-mb- u-font-size-12">
                        <svg
                            width="8"
                            height="10"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 8 10"
                            className="c-svg c-svg--w10 u-mr--"
                        >
                            <path
                                d="M7.805 4.744a.636.636 0 0 0-.472-.199h-.222V3.182c0-.871-.305-1.62-.917-2.244C5.584.313 4.852 0 4 0c-.852 0-1.583.313-2.195.938A3.097 3.097 0 0 0 .89 3.182v1.363H.667a.636.636 0 0 0-.473.2.665.665 0 0 0-.194.482v4.091c0 .19.065.35.194.483.13.133.288.199.473.199h6.666a.636.636 0 0 0 .473-.199A.666.666 0 0 0 8 9.318v-4.09c0-.19-.065-.351-.195-.484zm-2.027-.199H2.222V3.182c0-.502.174-.93.521-1.286A1.694 1.694 0 0 1 4 1.364c.49 0 .91.177 1.257.532.347.355.52.784.52 1.286v1.363z"
                                fill="#221F20"
                                fillRule="nonzero"
                            />
                        </svg>
                        Pay securely with:
                    </div>
                    <img
                        src={get(images, 'cards')}
                        alt="Supported cards"
                        srcSet={`${get(images, 'cards_2x')} 2x`}
                    />
                </div>
            </div>
        );
    }
}

export default SubscriptionOrderReview;
