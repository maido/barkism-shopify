/**
 * @prettier
 * @flow
 */
import * as React from 'react';

type Props = {
    children: Function,
    isActive?: boolean,
    isVisible?: boolean
};

const SubscriptionFormStep = ({children, isActive = true, isVisible = true}: Props) => (
    <div
        className="u-mb u-mb+@mobile u-relative u-fade-in js-step"
        data-active={isVisible}
        style={{
            display: isVisible ? 'block' : 'none',
            opacity: isActive ? 1 : 0.4
        }}
    >
        {children ? children(isActive) : null}
        <div
            className="js-step-blocker"
            style={{
                backgroundColor: 'transparent',
                bottom: 0,
                height: '100%',
                left: 0,
                position: 'absolute',
                width: '100%',
                visibility: isActive ? 'hidden' : 'visible'
            }}
        />
    </div>
);

export default SubscriptionFormStep;
