/**
 * @prettier
 * @flow
 */
import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';

type Props = {
    history: Object,
    links: Array<{
        key?: string,
        label: string,
        path: string
    }>
};

class SelectNavigation extends Component<Props> {
    handleOptionChange = (event: SyntheticInputEvent<HTMLSelectElement>) => {
        const path = event.target.value;

        if (typeof window !== 'undefined') {
            event.target.blur();

            this.props.history.push(path);
        }
    };

    render() {
        const {history, links} = this.props;
        return (
            <div className="c-select-navigation-wrapper">
                <select className="c-select-navigation" onChange={this.handleOptionChange}>
                    {links.map((link, index) => (
                        <option key={index} value={link.path}>
                            {link.label}
                        </option>
                    ))}
                </select>
            </div>
        );
    }
}

export default withRouter(SelectNavigation);
