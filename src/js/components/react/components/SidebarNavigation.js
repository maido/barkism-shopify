/**
 * @prettier
 * @flow
 */
import React from 'react';
import {NavLink} from 'react-router-dom';
import find from 'lodash/find';

type Props = {
    notifications: Array<Notification>,
    links: Array<{
        key?: string,
        label: string,
        path: string
    }>
};

const hasNotification = (notifications, key) => find(notifications, {key});

const SidebarNavigation = ({notifications, links = []}: Props) => (
    <nav className="c-sidebar-nav">
        {links.map((item, index) => {
            if (item.path.includes('http')) {
                return (
                    <a href={item.path} key={index} className="c-sidebar-nav__link">
                        {item.label}

                        {hasNotification(notifications, item.key) ? (
                            <span className="u-notification-icon u-ml--" />
                        ) : null}
                    </a>
                );
            } else {
                return (
                    <NavLink
                        key={index}
                        to={item.path}
                        className="c-sidebar-nav__link"
                        activeClassName="c-sidebar-nav__link--active"
                    >
                        {item.label}

                        {hasNotification(notifications, item.key) ? (
                            <span className="u-notification-icon u-ml--" />
                        ) : null}
                    </NavLink>
                );
            }
        })}
    </nav>
);

export default SidebarNavigation;
