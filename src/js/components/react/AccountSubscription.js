/**
 * @prettier
 * @flow
 */
import React, {Fragment} from 'react';
import SignUpCTA from './components/SignUpCTA';

type Props = {
    manageURL: string | null,
    status: string
};

const AccountSubscription = ({manageURL, status}: Props) => (
    <Fragment>
        <h2 className="u-heading u-mb u-mb+@mobile">Your subscription</h2>

        <section className="t-white u-shadow u-p u-p+@mobile">
            <p>
                {status === 'active' ? 'You are signed up to the monthly subscription.' : ''}
                {status === 'inactive'
                    ? 'Your subscription is currently inactive. You can activative it again at any time.'
                    : ''}
                {status === 'none' ? 'You have no subscriptions.' : ''}
            </p>

            {status !== 'none' ? (
                <a href={manageURL} className="c-button c-button--primary">
                    Manage your subscription
                </a>
            ) : (
                <SignUpCTA />
            )}
        </section>
    </Fragment>
);

export default AccountSubscription;
