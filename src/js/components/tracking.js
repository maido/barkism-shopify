/**
 * @prettier
 * @flow
 */
import GAnalytics from 'ganalytics';
import get from 'lodash/get';
import {intendedTargetElement} from '../services/helpers';

type TrackingElements = {
    $events?: NodeList<HTMLElement>
};

const els: TrackingElements = {};
let ga;

export const track = (type: string, parameters: Object) => {
    // console.log(type, parameters);

    ga.send(type, parameters);
};

const handleElementClick = (event: Event) => {
    const $target: HTMLElement = (intendedTargetElement('js-track-event', event.target): any);

    if ($target) {
        const ec = get($target, 'dataset.category');
        const el = get($target, 'dataset.label');

        if (ec && el) {
            track('event', {ec, el});
        }
    }
};

const cache = () => {
    els.$events = ((document.querySelectorAll('.js-track-event'): any): NodeList<HTMLElement>);
};

const events = () => {
    if (els.$events) {
        [...els.$events].map($event => $event.addEventListener('click', handleElementClick));
    }
};

const setupGA = () => {
    if (typeof window !== 'undefined') {
        if (window._ga) {
            ga = window._ga;
        } else {
            ga = new GAnalytics(get(window, 'globalData.gaID', ''));
            window._ga = ga;
        }
    }
};

const init = (listen: boolean = true) => {
    setupGA();

    if (ga && listen) {
        cache();
        events();
    }
};

export default {init};
