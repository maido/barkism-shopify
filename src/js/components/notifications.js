/**
 * @prettier
 * @flow
 */
import get from 'lodash/get';
import reject from 'lodash/reject';

type NotificationElements = {
    $notificationIcons?: NodeList<HTMLElement>
};

const els: NotificationElements = {};

const toggleNotifications = (visible: boolean) => {
    if (els.$notificationIcons) {
        [...els.$notificationIcons].map(
            $icon => ($icon.style.display = visible ? 'inline-block' : 'none')
        );
    }
};

export const clearNotification = (key: string) => {
    const notifications = readNotifications();

    if (notifications) {
        const updatedNotifications = reject(notifications, {key});

        if (updatedNotifications.length === 0) {
            toggleNotifications(false);
        }

        window.globalData.notifications = updatedNotifications;

        return updatedNotifications;
    }
};

export const readNotifications = () => get(window, 'globalData.notifications', []);

const cache = () => {
    els.$notificationIcons = ((document.querySelectorAll('.js-notification-icon'): any): NodeList<
        HTMLElement
    >);
};

const init = () => {
    cache();

    if (els.$notificationIcons) {
        const notifications = readNotifications();

        if (notifications.length) {
            toggleNotifications(true);
        }
    }
};

export default {init};
