/**
 * @prettier
 * @flow
 */
import anime from 'animejs';

const dogSizesGuide = {
    open() {
        anime
            .timeline()
            .add({
                duration: 0,
                opacity: 0,
                targets: '[data-modal="dog-sizes-guide"] .js-dog-breed'
            })
            .add({
                duration: 500,
                elasticity: 100,
                offset: '+=450',
                opacity: [0, 1],
                targets: '[data-modal="dog-sizes-guide"] .js-title',
                translateY: [24, 0]
            })
            .add({
                duration: 500,
                elasticity: 100,
                opacity: [0, 1],
                offset: '-=350',
                targets: '[data-modal="dog-sizes-guide"] .js-label',
                translateY: [6, 0]
            })
            .add({
                duration: 1200,
                delay: (target, index) => index * 50,
                elasticity: 100,
                offset: '-=450',
                opacity: [0, 1],
                targets: '[data-modal="dog-sizes-guide"] .js-dog-breed',
                translateY: [6, 0]
            });
    }
};

const boxSizesFallback = {
    open() {
        anime
            .timeline()
            .add({
                duration: 500,
                elasticity: 100,
                offset: '+=450',
                opacity: [0, 1],
                targets: '[data-modal="box-sizes-fallback"] .js-title',
                translateY: [24, 0]
            })
            .add({
                duration: 500,
                elasticity: 100,
                opacity: [0, 1],
                offset: '-=350',
                targets: '[data-modal="box-sizes-fallback"] .js-copy',
                translateY: [24, 0]
            })
            .add({
                duration: 1500,
                elasticity: 100,
                opacity: [0, 1],
                offset: '-=400',
                targets: '[data-modal="box-sizes-fallback"] .js-form',
                translateY: [6, 0]
            });
    }
};

const giftSubscriptionReview = {
    open() {
        anime
            .timeline()
            .add({
                duration: 500,
                elasticity: 100,
                offset: '+=450',
                opacity: [0, 1],
                targets: '[data-modal="gift-subscription-review"] .js-title',
                translateY: [24, 0]
            })
            .add({
                duration: 500,
                elasticity: 100,
                opacity: [0, 1],
                offset: '-=350',
                targets: '[data-modal="gift-subscription-review"] .js-copy',
                translateY: [24, 0]
            })
            .add({
                duration: 1500,
                elasticity: 100,
                opacity: [0, 1],
                offset: '-=400',
                targets: '[data-modal="gift-subscription-review"] .js-cta',
                translateY: [12, 0]
            });
    }
};

export default {
    'box-sizes-fallback': boxSizesFallback,
    'dog-sizes-guide': dogSizesGuide,
    'gift-subscription-review': giftSubscriptionReview
};
