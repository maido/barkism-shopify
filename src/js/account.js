/**
 * @prettier
 * @flow
 */
import React from 'react';
import {render} from 'react-dom';
import {HashRouter} from 'react-router-dom';
import get from 'lodash/get';
import Account from './components/react/Account';
import tracking, {track} from './components/tracking';
import {isMobile} from './services/helpers';

const trackLoadTime = () => {
    if (typeof window !== 'undefined' && window.performance) {
        const timeSinceLoad = Math.round(performance.now());

        tracking.init(false);
        track('timing', {utc: 'Account form render', utt: timeSinceLoad});
    }
};

const init = () => {
    const $container = document.querySelector('#account-container');
    const accountContent = get(window, 'globalData.accountContent');
    const customerId = get(window, 'globalData.customerId', 0);

    if ($container && accountContent) {
        trackLoadTime();

        render(
            <HashRouter>
                <Account {...accountContent} customerId={customerId} />
            </HashRouter>,
            $container
        );
    } else {
        alert('Sorry, something went wrong. Please refresh and try again');
    }
};

if (window.isModernBrowser) {
    window.addEventListener('load', init);
}
