/**
 * @prettier
 * @flow
 */
import equalHeights from './components/equal-heights';
import tabList from './components/tab-list';

const init = () => {
    equalHeights.init();
    tabList.init();
};

if (window.isModernBrowser) {
    window.addEventListener('load', init);
}
