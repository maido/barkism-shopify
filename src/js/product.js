/**
 * @prettier
 * @flow
 */
import React from 'react';
import {render} from 'react-dom';
import get from 'lodash/get';
import Subscription from './components/react/Subscription';
import tracking, {track} from './components/tracking';
import {isMobile} from './services/helpers';

const trackLoadTime = () => {
    if (typeof window !== 'undefined' && window.performance) {
        const timeSinceLoad = Math.round(performance.now());

        tracking.init(false);
        track('timing', {utc: 'Subscription form render', utt: timeSinceLoad});
    }
};

const init = () => {
    const $container = document.querySelector('#product-subscription-container');
    const subscriptionContent = get(window, 'globalData.subscriptionContent');
    const subscriptionProducts = get(window, 'globalData.subscriptionProducts');

    if ($container && subscriptionContent) {
        trackLoadTime();

        render(
            <Subscription {...subscriptionContent} subscriptionProducts={subscriptionProducts} />,
            $container
        );
    } else {
        alert('Sorry, something went wrong. Please refresh and try again');
    }
};

if (window.isModernBrowser) {
    window.addEventListener('load', init);
}
