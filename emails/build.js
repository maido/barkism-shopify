/**
 * @prettier
 */

const fs = require('fs');

const createTemplate = (filename, contents) => {
    fs.readFile('./layout.html', 'utf8', (err, layoutContents) => {
        if (err) {
            console.error(err);
            return;
        }

        layoutContents = layoutContents.replace('{HEADING}', contents.heading);
        layoutContents = layoutContents.replace('{PHOTO}', contents.photo);
        layoutContents = layoutContents.replace('{PREVIEW_CONTENT}', contents.previewContent);
        layoutContents = layoutContents.replace('{CONTENT}', contents.content);

        fs.writeFile(`./compiled/${filename}`, layoutContents, err => {
            if (err) {
                console.error(err);
            } else {
                console.log(`${filename} created`);
            }
        });
    });
};

const formatContent = content => content.replace(/(\r\n|\n|\r)/gm, '');

const getTemplateContent = filename => {
    fs.readFile(`./${filename}`, 'utf8', (err, contents) => {
        if (err) {
            console.error(err);
            return;
        }

        const content = contents.split('\n||\n');
        const formattedContent = {
            photo: formatContent(content[0]),
            heading: formatContent(content[1]),
            previewContent: formatContent(content[2]),
            content: formatContent(content[3])
        };

        createTemplate(filename, formattedContent);
    });
};

const getTemplates = () => {
    fs.readdir('./', (err, items) => {
        const templates = items.filter(i => i.includes('.html')).filter(i => !i.includes('layout'));

        templates.map(getTemplateContent);
    });
};

(() => {
    getTemplates();
})();
