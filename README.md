# Barkism - Shopify

Barkism.com is a Shopify store built to sell monthly subscriptions of Barkism boxes. Users can also purchase gift subscriptions. Subscriptions are handled via a third party Shopify app called ReCharge.

## ⬇️ Installing / Getting started

To get started, `cd` into your working/dev folder.

```shell
git clone git@bitbucket.org:maido/barkism-shopify.git
cd barkism/shopfiy
```

### Shopify Theme

To start developing the theme, you must add the necessary API passwords in `./config.yml` for both staging and production stores. This allows you to authorise and sync content and source files between your local and live (staging or production) environments.

To find the passwords, follow these steps:

- Navigate to the store's admin
- Click 'Apps' from the main navigation on the left
- Click the 'Manage private apps' link at the bottom of the right column, displayed under the app listing
- Click the name of the private app 
- Copy and paste the password from the 'Admin API' section into the relevant part of the config.yml


## 🛠 Front-End Stack

### HTML/Templating
Shopify uses [Liquid](https://help.shopify.com/en/themes/liquid) for theme templates. Liquid is a fairly straightforward language to use, but sometimes can be verbose compared to other languages. It is written in Ruby and is similar to Python's Jinja templating language.

### JavaScript
- Compiled with Webpack and Babel
- ES6 Modules
- ES6 features (const, let, spread, rest, arrow functions, template literals)
- [Flow static typing](https://flow.org/)
- React
- React Router
- Enzyme
- Node
- Express
- Shopify API

### CSS
- Compiled with Webpack and NodeSass
- [Inuit CSS](https://github.com/inuitcss/inuitcss)
- [Namespacing](https://csswizardry.com/2015/03/more-transparent-ui-code-with-namespaces/)
- PostCSS
   - [Autoprefixer](https://www.npmjs.com/package/autoprefixer)
   - [MQPacker](https://www.npmjs.com/package/css-mqpacker)

We use a combination of BEM and functional CSS (a la [Tachyons](https://github.com/tachyons-css/tachyons)) for building reusable, composable CSS. Where there is feature-specific styling we will create modules that follow BEM principles, but otherwise we use the functional CSS part. E.g. specific classes for flex, padding, margin, background colours etc. The prefixing/namespacing (`c-` for components, `u-` for utility classes, etc.) feature heavily throughout the code.

## 🛠 Back-End Stack
[CHANGE - What services and platforms does the project use? Document what a develoepr will need to know to work on the project].

### Language/Frameworks

### Database

## 💻 Developing

To work on Shopify themes locally you must have Shopify's [Slate](https://github.com/Shopify/slate) tools installed. This is a powerful CLI that includes testing and deployment to your Shopify store and it is *very* helpful.

For all the required information on Slate, please [refer to the official documentation](https://shopify.github.io/slate/).

```
⚠️ Slate is currently being upgraded to version 4, which is a rewrite of the tool.
This update brings some much needed improvements, but it's also more difficult to
integrate with our custom Webpack scripts.

The installed version of Slate should remain at version 3 until this is fixed.
```

### Linting

The project uses [Flow](https://flow.org/) and [Prettier](https://github.com/prettier/prettier). These are tools that can be used from the command line and/or as part of a build system, but work even better when use with your favourite editor (i.e. Sublime) as they format and validate your code as you type/save.

These instructions are for Sublime, but there are alternatives available for other editors or IDEs too.

- [http://www.sublimelinter.com/en/stable/](http://www.sublimelinter.com/en/stable/)
- [https://github.com/SublimeLinter/SublimeLinter-flow](https://github.com/SublimeLinter/SublimeLinter-flow)
- [https://github.com/jonlabelle/SublimeJsPrettier](https://github.com/jonlabelle/SublimeJsPrettier)	

### Running
We don't use Slate's JS or Sass tools so we need to run both our Webpack tools and Slate's tools together. Instead of the standard slate start command, we need to instead run:

`yarn develop`

This will start the Shopify content syncing and Webpack will begin watching for changes.

```
⚠️ When developing locally, any changes are automatically synced with the
Shopify store, so it's important to only work on the staging store in case any
breaking changes are introduced to the production store.
```

### Tests

Unit tests for the React code are written with [Jest](https://jestjs.io/). You can run the tests with:

`yarn test -u`

There is also CI integration with Bitbucket Pipelines. 


## 🚀 Deploying code

### Shopify theme

To build a production-ready version of the theme you need to run a single command which will then compile and minify all your JavaScript and Sass and then deploy your code to the defined store environment. For example:

`yarn deploy:production`

All assets are then synced with Shopify's CDN.

#### API
The API that powers the user account features is hosted on the [Now](https://zeit.co/docs) cloud hosting platform. All deployments to Now are made via its official CLI. For more information on set up, please [refer to the docs](https://zeit.co/docs/v2/getting-started/installation/).

## 🗃 Content Management

Content is edited in 2 places on a Shopify store. The main built-in content types that are edited from the Shopify admin are Collections, Products, Blogs and Pages.

Any additional content is edited via the Theme Editor (accessible via Online Store section of the admin). Custom content is created via [Sections](https://help.shopify.com/en/themes/development/sections), with each section have its own content model.

### Access credentials
User accounts are available via LastPass in the shared project folder.


## 🌟 Notable features

### Subscriptions

Subscriptions are handled via a third party Shopify app called [ReCharge](https://rechargepayments.com/). During installation of the app, ReCharge will also install custom theme files which have the code for allowing users to pick subscription and define their preferences on duration, etc. We don't use this UI in our sign-up journey, but we do rely on all of the JavaScript that is added to get the necessary information for adding subscription products to cart.

Some other notes on ReCharge:

- All administration and configuration is done via the ReCharge app. This can be found by logging into the Shopify admin and then navigating to 'Apps'.
- ReCharge uses its own checkout service, that is similar, but different to the native Shopify checkout experience. This is required to store user card details for monthly charges.
- ReCharge's support team are very helpful and will take the time to help if you email directly to their support team.


### Bespoke Subscription sign-up
The full journey for both subscriptions and gift are wrote in React. There is no support for the sign-up without JavaScript, so the only code is in React, there is no HTML + JS progressive enhancements.

All subscriptions are powered by ReCharge. ReCharge has some custom templates that output lots of information (ids, renewals, etc.) into a JavaScript variable which can then be read in React. The information that ReCharge shows is based on the product you visit (e.g. https://www.barkism.com/products/monthly-subscription), but the subscription UI allows a user to change to a different product (subscription vs gift, then 1 month vs 2 month gift) so we have some extra information stored on the `globalData` object that contains all required information for all subscription products. By doing this we can easily pass in the right information to ReCharge when adding to cart.


### Bespoke user account management
Users can save additional details on their dog (only support for one just now!). Out of the box, Shopify doesn't allow us to store additional information on users. But, using [Metafields](https://help.shopify.com/en/manual/products/metafields) we can 'associate' extra information to a user.


## 👋 Contributing

If you are adding a new feature or fix to the project please follow these steps:

- Checkout master and pull all latest changes
- Create a new branch (`git checkout -b (fix|feature)/[ticket number]_[branch name]`). For example: `git checkout -b fix/MD-123_timeout-error`.
- Do your typing and write your code
- When ready to go, create a Pull Request to Staging detailing the changes made with an updated Changelog and Readme if required
- Once code reviewed, deploy changes to the Staging environment and test there
- Once approved, create a Pull Request to Master
- Once code reviewed, deploy changes to Production environment
- Smoke test changes on Production environment
- Done!


## 🤓 Further reading

Here is some helpful links for important Shopify theme development features:

- [Theme structure](https://help.shopify.com/en/themes/development/templates) - This outlines the important architecture of templates, sections and snippets.
- [Theme sections](https://help.shopify.com/en/themes/development/sections) - This is how create content-managed 'sections' for our pages
- [Navigation](https://help.shopify.com/en/themes/development/building-nested-navigation) - We use a few single and nested navigations around the site
- [Liduid template basics](https://help.shopify.com/en/themes/liquid/basics) - Basic need-to-know for using Liquid templates
- [Liquid objects](https://help.shopify.com/en/themes/liquid/objects) - Liquid objects contain all the information needed to get data from your store to your HTML (e.g. product information, settings)
- [Liquid tags](https://help.shopify.com/en/themes/liquid/tags) - These are important for building your templates, including control flow and iteration
- [Liquid filters](https://help.shopify.com/en/themes/liquid/filters) - Filters are used to manipulate the information stored in objects
- [Slate](https://shopify.github.io/slate/) - This is the CLI tool we use for syncing content and files between our local development environment and a live store

Shopify advanced features:

- [Metafields](https://help.shopify.com/en/api/reference/metafield) - How we can store additional information against a product, user, etc. that builds upon the content model that exists out of the box
- [Shopify Scripts](https://help.shopify.com/en/manual/apps/apps-by-shopify/script-editor/shopify-scripts) - These are Ruby-based scripts that can affect checkouts with things like money off, free shipping or free products.
- [Shopify Ajax API](https://help.shopify.com/en/themes/development/getting-started/using-ajax-api) - Creating improved UX during checkout and cart processes.

ReCharge payments:

- [Official documentation](https://support.rechargepayments.com/hc/en-us)
- [API access and documentation](https://rechargepayments.com/developers)



## 💬 Discussion

You can join the project's [Slack channel](https://maidoteam.slack.com/messages/CACABHEA2) for any discussion.



## ReCharge

window is used to get global functions set in the template into react templates, eg:
- process function that redirects to checkout
- products and their subscription information

template snippet is used to get the product's subscription info into the template
`subscription-product`
